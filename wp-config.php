<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'beechnurserywest' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'CW+!]*=#gKfzU.@-wjfm#DeTt!c>@d.|@t~-WF`l;xK2~NIVDtz$>1i#1z7U(;O3' );
define( 'SECURE_AUTH_KEY',  '%Sgd#<gKDM3gSZ`[uh/*<wQ6P1?c~+ph3nu`&ew:T4p^<p%Hy&,k(wzo*}BBw7Y[' );
define( 'LOGGED_IN_KEY',    'l6kY|)Y4T7hk?_b /l~&)P<a7LNv87}ayngY3^RQ3zZ,~/lMfr(aKT12WArcfsI)' );
define( 'NONCE_KEY',        '1R:d`{LNuy@<&3zEo.xo};KK<Vt%R8@VJhN` ,oLhRah#U5<SR/V&%YTBcj+L,O5' );
define( 'AUTH_SALT',        '(G{6eTEvb=-:,Pivi+=NnmH>|ml!jtBtC:@(_jGGW(|-P@d~#$M=^|4v+KS/qhH+' );
define( 'SECURE_AUTH_SALT', 'L$(kJoHgWWpYFHY<4hZ*F*hp,jq}K*sqxLwI$!jH]ocyrZv(a!s5-iWnqD;)I#QM' );
define( 'LOGGED_IN_SALT',   '$RA0F$=f;*2_|`HgxxB%ioQ2dKpRc]VwcX]l*H=D#-0lS^Y3 bs&OfWb@rDj7Tvl' );
define( 'NONCE_SALT',       'WbVe~*.YtX$>$YVIsu#+.6T_Bq@/puN7gZLYv66{U~?`|50pfj2]~h4kPEw;9ew}' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
