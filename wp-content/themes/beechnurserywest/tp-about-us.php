<?php
/**
* Template Name: About Us Template
*/

get_header();
?>

<div class="about-us">
    <div class="custom-header-area">
        <div class="title"><h1><span class="blue">About</span> Us</h1></div>
    </div>
    <div class="page-content">
        <div class="site-wrapper">
            <div class="row">
                <div class="col-md-4">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/about-maple-tree.jpg" alt="Maple Tree"/>
                </div>
                <div class="col-md-8">
                    <h2 class="intro-title"><span class="thin">Welcome to</span><br/><span class="thick"><span class="blue">BEECH</span> NURSERY WEST</span></h2>
                    <p class="intro-content">
                        BEECH NURSERY WEST is focused on customer service satisfaction, whether you are a large commercial developer, professional landscape contractor or avid gardener. We have a large fleet that can deliver all products as per your order. We proudly serve landscape contractors, garden centres, municipalities and golf courses.
                    </p>
                    <p class="intro-content">
                        BEECH NURSERY WEST’s convenient and cost saving delivery fleet includes large boom trucks and trailers, small trucks, and enclosed dry vans. They are available daily to deliver material directly to your job site or yard.
                    </p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="stats">
                                <div class="number">25+</div>
                                <div class="stat-type">Years of Experience</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="stats">
                                <div class="number">200+</div>
                                <div class="stat-type">Acres of Nursery Space</div>
                            </div>
                        </div>
                        <!-- <div class="col-md-4">
                            <div class="stats">
                                <div class="number">XX</div>
                                <div class="stat-type">Insert Subtitle Here</div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="faq-content-wrapper">
                <div class="faq-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="faq-title">History</div>
                            <br/>
                        </div>
                        <div class="col-md-12">
                            <p>
                                In 2008 Alex Emerita recognized to better service small to mid-size landscapers, so he started his first company Plants 4 Less. Starting and learning this business came with help from his brother-in-law Rick Borges who had been working at a large Local Nursery for-more than 20 years. After growing the business for 5 years, Alex and Rick founded Beech Nursery West on a 17 acre property west of Highway 400 in Schomberg, Ontario. Beech Nursery West began as a wholesale nursery serving industry professionals. Today, we’re still a wholesale nursery, and now also a grower. Our mission remains the same—to offer our customers of any size more possibilities with a higher quality of service than any other wholesale nursery. We now draw from a combined 250 acres of nursery space and distribution yards. We are constantly expanding product line and consistently evolving better to serve our customers.
                            </p>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="faq-title">FAQ</div>
                            <div class="faq-content">
                                <p><b>Question: </b>Do you warranty your material?</p>
                                <p><b>Answer: </b>We guarantee delivery of healthy and viable material to your job site or location. Plant material usually only suffers from poor handling or insufficient watering. Once you have accepted your items it is your responsibility to ensure their survival. Please report any unsatisfactory stock within 48 hours of receipt of items.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="faq-title">FAQ</div>
                            <div class="faq-content">
                                <p><b>Question: </b>How quickly can we expect delivery of our items?</p>
                                <p><b>Answer: </b>Providing we have your material in stock, we can usually ship your items within 24 and 72 hours. Please contact our sales staff to secure your spot.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="faq-title">FAQ</div>
                            <div class="faq-content">
                                <p><b>Question: </b>Can I pick up my items at BEECH NURSERY WEST?</p>
                                <p><b>Answer: </b>Yes! Please try to give us 24 hours' notice so we can pick and prepare your order for you.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="landscape-ontario">
        <div class="site-wrapper">
            <div class="title">Members of Landscape Ontario</div>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/landscape-ontario-logo.png" alt="Landscape Ontario Logo" />
        </div>
    </div>
    <div class="management-team">
        <div class="site-wrapper">
            <div class="management-title">Our <span class="blue">Management</span> Team</div>
            <div class="row">
                <div class="col-md-4">
                    <div class="person">
                        <div class="person-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/alex-image.jpg" alt="Alex Emerita" />
                        </div>
                        <div class="person-name">
                            Alex Eremita
                        </div>
                        <div class="person-title">
                            Chief Sales & Operations Manager
                        </div>
                        <!-- <div class="person-content">
                            Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum.
                        </div> -->
                        <div class="person-email">
                            <span class="green"><i class="far fa-envelope"></i>Email: </span>alex@beechnurserywest.com
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="person">
                        <div class="person-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/rick-image.jpg" alt="Alex Emerita" />
                        </div>
                        <div class="person-name">
                            Rick Borges
                        </div>
                        <div class="person-title">
                            General Manager
                        </div>
                        <!-- <div class="person-content">
                            Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum.
                        </div> -->
                        <div class="person-email">
                            <span class="green"><i class="far fa-envelope"></i>Email: </span>rick@beechnurserywest.com
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-4">
                    <div class="person">
                        <div class="person-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/female-image.jpg" alt="Alex Emerita" />
                        </div>
                        <div class="person-name">
                            Person Person
                        </div>
                        <div class="person-title">
                            Person title
                        </div>
                        <div class="person-content">
                            Sed nec felis pellentesque, lacinia dui sed, ultricies sapien. Pellentesque orci lectus, consectetur vel posuere posuere, rutrum eu ipsum.
                        </div>
                        <div class="person-email">
                            <span class="green"><i class="far fa-envelope"></i>Email: </span>person@person.com
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="contact-promo">
        <div class="site-wrapper">
            <div class="catalog-content">
                <p>Questions? Feel free to reach out to us, we are here to help!</p><a href="<?php echo get_site_url(); ?>/contact" class="custom-button white">Contact Us</a>
            </div>
        </div>
    </div>
    <div class="group-of-companies">
        <div class="site-wrapper">
            <div class="title">We Are A <span class="blue"><span class="thick">Group</span></span> Of Companies</div>
        </div>
        <div class="custom-flex-wrapper">
            <div class="company corporate">
                <div class="company-title"><span class="blue">BEECH </span>Nursery Group</div>
                <?php
                    $beechGroup = get_field('beech_nursery_group_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $beechGroup; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://www.beechnurserygroup.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
            <div class="company toronto">
                <div class="company-title"><span class="blue">BEECH </span>Nursery Toronto</div>
                <?php
                    $beechToronto = get_field('beech_nursery_toronto_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $beechToronto; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://beechnursery.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
            <div class="company spading">
                <div class="company-title"><span class="blue">HALL </span>Tree Spading</div>
                <?php
                    $hallSpading = get_field('hall_tree_spading_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $hallSpading; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://halltreespading.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
