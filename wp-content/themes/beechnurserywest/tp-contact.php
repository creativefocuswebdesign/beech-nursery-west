<?php
/**
 * Template Name: Contact Page Template
 */

get_header();
?>

<div class="contact-page">
    <div class="custom-header-area">
        <div class="title"><h1>Contact <span class="blue">Us</span></div>
    </div>
    <div class="content">
        <div class="site-wrapper">
            <div class="introduction">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Get in <span class="green">Touch</span></h3>
                    </div>
                    <!-- <div class="col-md-8">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div> -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h3>Need our services or Have a question ?</h3>
                    <div class="information-wrapper">
                        <div class="info">
                            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-1.png" alt="Icon"/></div>
                            <div class="content"><span class="title thick">Address </span>5518 Highway 9<br/>Schomberg, ON,L0G 1T0<br/>Canada</div>
                        </div>
                        <div class="info">
                            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-1.png" alt="Icon"/></div>
                            <div class="content"><span class="title thick">Contact Details </span>905 . 939 . 8733<br/>Alex@beechnurserywest.com<br/>Rick@beechnurserywest.com</div>
                        </div>
                        <div class="info">
                            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-1.png" alt="Icon"/></div>
                            <div class="content"><span class="title thick">Working Hours</span>
                                <div>
                                    <span>April</span>
                                </div>
                                <div class="hours-content"><span class="day">Mon - Fri</span>8:00AM - 4:00PM</div>
                                <div>
                                    <span>May-Dec</span>
                                </div>
                                <div class="hours-content"><span class="day">Mon - Fri</span>7:00AM - 6:00PM</div>
                                <div class="hours-content"><span class="day">Saturdays</span>8:00AM - 2:00PM</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="contact-form-wrapper">
                        <?php echo do_shortcode('[contact-form-7 id="28" title="Homepage Contact Form"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="catalog-download">
        <div class="site-wrapper">
            <div class="catalog-content">
                <p>Interested in our wholesale catalogue?</p><a href="<?php echo get_site_url(); ?>/catalog" class="custom-button white">Request Catalogue</a>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
