<?php
/**
 * Template Name: Products Template
 */

get_header();

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

?>

<div class="products-page">
    <div class="custom-header-area" style="background-image:url('<?php echo $featured_img_url; ?>')">
        <div class="title"><h1><span class="blue">Products</span> - <?php echo $post->post_title; ?></div>
    </div>

    <div class="product-content">
        <div class="site-wrapper">
            <div class="intro-content">
                <p><?php the_field('introduction_paragraph_text'); ?></p>
            </div>
            <div class="photo-gallery">

                <?php
        		while ( have_posts() ) :
        			the_post();
                    echo do_shortcode($post->post_content);
        		endwhile; // End of the loop.
        		?>

            </div>

            <div class="split-content">
                <div class="row">
                    <div class="col-md-6">
                        <?php the_field('product_list_left_side'); ?>
                    </div>
                    <div class="col-md-6">
                        <?php the_field('product_list_right_side'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="catalog-download">
        <div class="site-wrapper">
            <div class="catalog-content">
                <p>Interested in our wholesale catalogue?</p><a href="<?php echo get_site_url(); ?>/catalog" class="custom-button white">Request Catalogue</a>
            </div>
        </div>
    </div>

</div>

<?php
get_footer();
