<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package beechnurserywest
 */

?>

	</div><!-- #content -->


	<footer id="colophon" class="site-footer">
		<div id="gmap"></div>
		<div class="instagram-feed">
			<?php echo do_shortcode('[elfsight_instagram_feed id="1"]'); ?>
		</div>
		<div class="custom-footer">
			<div class="site-wrapper">
				<div class="row">
					<div class="col-md-3">
						<div class="footer-title"><span>About <span class="green">Us</span></span></div>
						<p>BEECH NURSERY WEST is focused on customer service satisfaction, whether you are a large commercial developer, professional landscape contractor or avid gardener. BEECH NURSERY WEST has a large fleet that can deliver all products as per your order.</p>
						<div class="button-area">
							<a class="custom-button green" href="<?php echo get_home_url(); ?>/contact">Contact Us Today</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="footer-title"><span>We Carry</span></div>
						<div class="list">
							<ul>
								<li>Shade/Flowering Trees</li>
								<li>Broadleaf Shrubs</li>
								<li>Broadleaf Evergreens</li>
								<li>Ornamental Shrubs</li>
								<li>Conifers</li>
								<li>Ground Covers</li>
								<li>Perennials</li>
							</ul>
						</div>
					</div>
					<div class="col-md-3">
						<div class="footer-title"><span>Useful Links</span></div>
						<div class="list">
							<ul>
								<li><a href="<?php echo get_home_url(); ?>">Home</a></li>
								<li><a href="<?php echo get_home_url(); ?>/about-us">About Us</a></li>
								<li><a href="<?php echo get_home_url(); ?>/services">Services</a></li>
								<li><a href="<?php echo get_home_url(); ?>/careers">Careers</a></li>
								<li><a href="<?php echo get_home_url(); ?>/contact/">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3">
						<div class="footer-title"><span>Get in Touch</span></div>
						<div class="contact-information">
							<div class="contact">
								<i class="fas fa-map-marker-alt"></i>
								<p>5518 Hwy 9 Schomberg, <br/>Ontario, L0G 1T0, Canada</p>
							</div>
							<div class="contact">
								<i class="fas fa-phone"></i>
								<p>905 . 939 . 8733</p>
							</div>
							<div class="contact">
								<i class="far fa-envelope"></i>
								<p>Alex@beechnurserywest.com</p>
							</div>
							<div class="contact">
								<i class="far fa-clock"></i>
								<p>M - F 7:00 AM - 6:00 PM<br/>
									SAT 8:00 AM - 2:00 PM<br/>
									SUN <span class="green">CLOSED</span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="bottom-footer">
							<div class="row">
								<div class="col-md-6">
									<p>&copy; Copyright 2019 Beech Nursery West</p>
								</div>
								<div class="col-md-6">
									<p class="text-right">Custom Web Design by <a href="http://www.creativefocuswebdesign.com" target="_blank">Creative Focus Web Design</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">

	var mobyMenu = new Moby({
	    menu       : jQuery('#mobify'), // The menu that will be cloned
		mobyTrigger: jQuery('#moby-button'), // Button that will trigger the Moby menu to open
	});

	function initMap() {

		// Beech West Location
		var myLatlng = new google.maps.LatLng(44.0155662,-79.6608589);

		// Map Setup
		var map = new google.maps.Map(document.getElementById('gmap'), {
			center: {lat: 44.0128791, lng: -79.6795788},
			zoom: 13,
			streetViewControl: false,
			mapTypeControl: false,
			fullscreenControl: false,
			styles: [
		    {
		        "featureType": "all",
		        "elementType": "labels.text.fill",
		        "stylers": [
		            {
		                "saturation": 36
		            },
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 40
		            }
		        ]
		    },
		    {
		        "featureType": "all",
		        "elementType": "labels.text.stroke",
		        "stylers": [
		            {
		                "visibility": "on"
		            },
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 16
		            }
		        ]
		    },
		    {
		        "featureType": "all",
		        "elementType": "labels.icon",
		        "stylers": [
		            {
		                "visibility": "off"
		            }
		        ]
		    },
		    {
		        "featureType": "administrative",
		        "elementType": "geometry.fill",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 20
		            }
		        ]
		    },
		    {
		        "featureType": "administrative",
		        "elementType": "geometry.stroke",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 17
		            },
		            {
		                "weight": 1.2
		            }
		        ]
		    },
		    {
		        "featureType": "landscape",
		        "elementType": "geometry",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 20
		            }
		        ]
		    },
		    {
		        "featureType": "poi",
		        "elementType": "geometry",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 21
		            }
		        ]
		    },
		    {
		        "featureType": "road.highway",
		        "elementType": "geometry.fill",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 17
		            }
		        ]
		    },
		    {
		        "featureType": "road.highway",
		        "elementType": "geometry.stroke",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 29
		            },
		            {
		                "weight": 0.2
		            }
		        ]
		    },
		    {
		        "featureType": "road.arterial",
		        "elementType": "geometry",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 18
		            }
		        ]
		    },
		    {
		        "featureType": "road.local",
		        "elementType": "geometry",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 16
		            }
		        ]
		    },
		    {
		        "featureType": "transit",
		        "elementType": "geometry",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 19
		            }
		        ]
		    },
		    {
		        "featureType": "water",
		        "elementType": "geometry",
		        "stylers": [
		            {
		                "color": "#000000"
		            },
		            {
		                "lightness": 17
		            }
		        ]
		    }
		]});

		// var contentString = '<div id="content">'+
		// '<h1 id="firstHeading" class="firstHeading">Beech Nursery Corporate</h1>'+
		// '<div id="bodyContent">'+
		// '<p></p>'+
		// '</div>'+
		// '</div>';

		// Custom infowindow with custom content
		// var infowindow = new google.maps.InfoWindow({
		// 	content: contentString,
		// 	maxWidth: 300
		// });

		// Custom Marker Setup
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			// title: 'Uluru (Ayers Rock)',
			icon: '<?php echo get_template_directory_uri(); ?>' + '/assets/images/map-marker.png'
		});

		// Click listener for custom marker
		// marker.addListener('click', function() {
		// 	infowindow.open(map, marker);
		// });

		// Add Marker to map
		marker.setMap(map);
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0mjZGPrSKQfeDl8HeGC2xetBSwzLZzSs&callback=initMap" async defer></script>

</body>
</html>
