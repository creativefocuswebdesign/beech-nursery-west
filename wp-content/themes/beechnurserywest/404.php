<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package beechnurserywest
 */

get_header();
?>
<div class="custom-header-area" style="background-color:#333333;">
	<div class="title"><h1><span class="blue">Error</span> 404</h1></div>
</div>
<section id="primary" class="content-area">
	<main id="main" class="site-main">
		<div class="site-wrapper" style="text-align:center;margin:0 auto;">
			<div class="error-404 not-found">
				<br/>
				<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentynineteen' ); ?></h1>
				<br/>
				<div class="page-content">
					<h4><?php _e( 'It looks like nothing was found at this location.', 'twentynineteen' ); ?></h4>
					<br/>
					<a href="<?php echo get_site_url(); ?>">Back To Homepage</a>
				</div><!-- .page-content -->
				<br/><br/>
			</div><!-- .error-404 -->
		</div>

	</main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();
