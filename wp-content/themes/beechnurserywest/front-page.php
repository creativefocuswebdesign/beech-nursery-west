<?php
/**
 * Template Name: Homepage Template
 */

get_header();
?>

<div class="homepage">
    <div class="intro-slider">
        <div class="slider-carousel owl-carousel owl-theme" id="slider-carousel">
            <div class="item" id="item-001" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/slide-image-1.jpg');">
                <div class="site-wrapper">
                    <h1>A wholesale <b>supplier</b>.</h1>
                    <p>Shade/flowering trees, broadleaf/ornamental shrubs, conifers/broadleaf evergreens, ground covers, perennials and ornamental shrubs.</p>
                    <div class="button-area">
                        <div class="row">
                            <div class="col-md-6"><a class="custom-button green" href="<?php echo get_site_url(); ?>/catalog">Request Catalogue</a></div>
                            <div class="col-md-6"><a class="custom-button white" href="<?php echo get_site_url(); ?>/contact">Contact Us</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="information-panel">
        <div class="site-wrapper">
            <div class="row">
                <div class="col-md-6">
                    <h2><span class="thin">Welcome to </span><span class="blue"><span class="thick">BEECH</span></span> <span class="thick">NURSERY WEST</span></h2>
                    <?php echo the_field('homepage_introduction_text'); ?>
                    <div class="button-area">
                        <a href="#"></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/beech-west-intro-image.jpg" alt="Beech Nursery West Entrance"/>
                </div>
            </div>
        </div>
    </div>
    <div class="catalog-download">
        <div class="site-wrapper">
            <div class="catalog-content">
                <p>Interested in our wholesale catalogue?</p><a href="<?php echo get_site_url(); ?>/catalog" class="custom-button white">Request Catalogue</a>
            </div>
        </div>
    </div>
    <div class="why-beech-west">
        <div class="site-wrapper">
            <div class="title"><h3>Why <span class="thick"><span class="blue">BEECH</span> NURSERY WEST</span> ?</h3></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="content-wrapper">
                        <div class="content">
                            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-1.png" alt="Icon"/></div>
                            <div class="title"><span>We Help You Succeed</span></div>
                            <div class="text"><p>Because everything we do at Beech Nursery West is about helping you succeed. We help your landscape design plans come to life with the finest plants available.</p></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content-wrapper">
                        <div class="content">
                            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-1.png" alt="Icon"/></div>
                            <div class="title"><span>Over 25 Years of Expeirence</span></div>
                            <div class="text"><p>With 25+years of experience in the industry let us help you meet your client’s requests by sourcing everything from common to hard-to-find plant specimens from our own fields or from our extensive network of vendors.</p></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content-wrapper">
                        <div class="content">
                            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-1.png" alt="Icon"/></div>
                            <div class="title"><span>Honesty & Respect</span></div>
                            <div class="text"><p>We want to serve you with respect and honesty, offering the best and most efficient service so you can get your job done on time.</p></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content-wrapper">
                        <div class="content">
                            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-1.png" alt="Icon"/></div>
                            <div class="title"><span>Wide Variety</span></div>
                            <div class="text"><p>Our goal is to provide the widest variety of plants to the landscape trade while establishing ourselves as the finest wholesale nursery in the country.</p></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="content-wrapper">
                        <div class="content">
                            <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-1.png" alt="Icon"/></div>
                            <div class="title"><span>Great Customer Service</span></div>
                            <div class="text"><p>We strive to provide the most compelling shopping experience possible through superior customer service, unbeatable yard service, fast load time, dependable on-time delivery service, expert product knowledge, and premium quality plants.</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="split-panel">
        <div class="split">
            <div class="slider-carousel owl-carousel owl-theme" id="split-carousel">
                <div class="item" id="item-001" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/home-gallery/taylor-juniper.jpg');">
                </div>
                <div class="item" id="item-002" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/home-gallery/limelight.jpg');">
                </div>
                <div class="item" id="item-003" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/home-gallery/ginkgo-tree.jpg');">
                </div>
                <div class="item" id="item-004" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/home-gallery/beech-west-trucks.jpg');">
                </div>
                <div class="item" id="item-005" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/home-gallery/basket-trees.jpg');">
                </div>
            </div>
        </div>
        <div class="split">
            <div class="title"><h4><span class="thick">Get Your <span class="green">Project</span> Started Today.</span></h4></div>
            <div class="contact-form-wrapper">
                <?php echo do_shortcode('[contact-form-7 id="28" title="Homepage Contact Form"]'); ?>
            </div>
        </div>
    </div>
    <div class="radio-promo-panel">
        <div class="site-wrapper">
            <div class="title"><span>"DOWN THE GARDEN PATH" RADIO SHOW</span></div>
            <p>This week I am joined by co-host Matthew Dressing as we talked to Alex Eremita from Beech Nursery West. Beech Nursery West is a wholesale nursery that was started by Alex and his partner Rick Borges in 2013. Have a listen to Beech Nursery West’s story I am sure you will enjoy it as much as we did!</p>
            <div class="button-area">
                <a href="https://www.down2earth.ca/beech-nursery-west-wholesale-nursery/" target="_blank" class="custom-button green">Listen To Broadcast</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery( document ).ready(function() {
    // Slider Panel
    jQuery('#slider-carousel').owlCarousel({
        loop:false,
        nav:true,
        dots: false,
        items:1,
        autoplay: false,
        autoplayTimeout:7500,
    });

    // Split Sider Panel
    jQuery('#split-carousel').owlCarousel({
        loop:true,
        nav:false,
        dots: false,
        items:1,
        autoplay: true,
        autoplayTimeout:7500,
    });
});
</script>

<?php
get_footer();
