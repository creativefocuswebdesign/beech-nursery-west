<?php
/**
 * Template Name: Careers Template
 */

get_header();
?>

<div class="careers-page">
    <div class="custom-header-area">
        <div class="title"><h1>Careers</div>
    </div>
    <div class="content">
        <div class="site-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="careers-content">

                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();

                        the_content();

                        endwhile; ?>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="contact-promo">
            <div class="site-wrapper">
                <div class="catalog-content">
                    <p>Questions? Feel free to reach out to us, we are here to help!</p><a href="<?php echo get_site_url(); ?>/contact" class="custom-button white">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
    <div class="group-of-companies">
        <div class="site-wrapper">
            <div class="title">We Are A <span class="blue"><span class="thick">Group</span></span> Of Companies</div>
        </div>
        <div class="custom-flex-wrapper">
            <div class="company corporate">
                <div class="company-title"><span class="blue">BEECH </span>Nursery Group</div>
                <?php
                    $beechGroup = get_field('beech_nursery_group_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $beechGroup; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://www.beechnurserygroup.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
            <div class="company toronto">
                <div class="company-title"><span class="blue">BEECH </span>Nursery Toronto</div>
                <?php
                    $beechToronto = get_field('beech_nursery_toronto_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $beechToronto; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://beechnursery.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
            <div class="company spading">
                <div class="company-title"><span class="blue">HALL </span>Tree Spading</div>
                <?php
                    $hallSpading = get_field('hall_tree_spading_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $hallSpading; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://halltreespading.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
