<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package beechnurserycorporate
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139914878-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-139914878-1');
	</script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel='shortcut icon' type='image/x-icon' href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon-west.png" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="default">
		<div class="top-header">
			<div class="site-wrapper">
				<div class="row">
					<div class="col-md-6"><p>Interested in our wholesale catalogue? <u><a href="<?php echo get_site_url(); ?>/catalog">Request Catalogue</a></u></p></div>
					<div class="col-md-6"><p class="text-right"><i class="far fa-envelope"></i>Alex@beechnurserywest.com<span class="separator">|</span>
						<i class="fas fa-mobile-alt"></i>905 . 939 . 8733</p></div>
				</div>
			</div>
		</div>
		<div class="header-wrapper">
			<div class="site-wrapper">
				<div class="flex-wrapper">
					<div class="logo-area">
						<a href="<?php echo get_home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/beech-nursery-west-logo.jpg" alt="Beech Nursery West Logo"/>
						</a>
					</div>
					<div class="company-tagline">
						<div class="info">
							<div class="inner-info">
								<i class="fas fa-map-marker-alt"></i>
								<p class="large">5518 Highway 9<br/><span class="small">Schomberg, ON, L0G 1T0</span></p>
							</div>
						</div>
						<div class="info">
							<div class="inner-info">
								<i class="far fa-clock"></i>
								<p class="large">Mon - Fri 8.00AM - 4.00PM<br/><span class="small">Sunday CLOSED</span></p>
							</div>
						</div>
						<div class="info">
							<div class="inner-info">
								<i class="far fa-envelope"></i>
								<p class="large">Alex@beechnurserywest.com</p>
							</div>
						</div>
						<div class="info">
							<div class="inner-info">
								<i class="fab fa-facebook-f"></i>
								<p class="large">facebook.com/beechnurserywest</p>
							</div>
						</div>
					</div>
					<div class="mobile-menu-button" id="moby-button">
						<i class="fas fa-bars"></i>
					</div>
				</div>
			</div>
			<div class="bottom-header-wrapper">
				<div class="menu-area" id="mobify">
					<?php
						// Output Menu
						$defaults = array(
							'menu' => 'main-menu',
						);
						wp_nav_menu( $defaults );
					?>
					<div class="telephone-area">
						<!-- <i class="fas fa-phone-volume"></i> -->
						<span class="telephone">905 . 939 . 8733</span>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
