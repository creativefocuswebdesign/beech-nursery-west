<?php
/**
* beechnurserywest functions and definitions
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*
* @package beechnurserywest
*/

if ( ! function_exists( 'beechnurserywest_setup' ) ) :
	/**
	* Sets up theme defaults and registers support for various WordPress features.
	*
	* Note that this function is hooked into the after_setup_theme hook, which
	* runs before the init hook. The init hook is too late for some features, such
	* as indicating support for post thumbnails.
	*/
	function beechnurserywest_setup() {
		/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on beechnurserywest, use a find and replace
		* to change 'beechnurserywest' to the name of your theme in all the template files.
		*/
		load_theme_textdomain( 'beechnurserywest', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'beechnurserywest' ),
		) );

		/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'beechnurserywest_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		* Add support for core custom logo.
		*
		* @link https://codex.wordpress.org/Theme_Logo
		*/
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'beechnurserywest_setup' );

/**
* Set the content width in pixels, based on the theme's design and stylesheet.
*
* Priority 0 to make it available to lower priority callbacks.
*
* @global int $content_width
*/
function beechnurserywest_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'beechnurserywest_content_width', 640 );
}
add_action( 'after_setup_theme', 'beechnurserywest_content_width', 0 );

/**
* Register widget area.
*
* @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
*/
function beechnurserywest_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'beechnurserywest' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'beechnurserywest' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'beechnurserywest_widgets_init' );

/**
* Enqueue scripts and styles.
*/
function custom_scripts() {
	//Jquery Slim
	wp_enqueue_script( 'jquery-slim',  'https://code.jquery.com/jquery-3.3.1.min.js', false );
	// Popper JS
	wp_enqueue_script( 'bootstrap-popper',  'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', false );
	// Bootstrap JS
	wp_enqueue_script( 'bootstrap-js',  'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', false );

	// Generic Theme Styles (twentynineteen)
	wp_enqueue_style( 'cfwd-generic-styles', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );
	wp_style_add_data( 'cfwd-generic-styles', 'rtl', 'replace' );

	// GoogleAPI Font
	wp_enqueue_style( 'google-montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:100,300,400,700,900', false );
	wp_style_add_data( 'google-montserrat', 'rtl', 'replace' );

	// FontAwesome
	wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css', false );
	wp_style_add_data( 'font-awesome', 'rtl', 'replace' );

	// Global Enqueue Owl Carousel
	wp_enqueue_style( 'owl-carousel-core', get_template_directory_uri().'/assets/css/owl.carousel.min.css', false );
	wp_enqueue_style( 'owl-carousel-default', get_template_directory_uri().'/assets/css/owl.theme.default.css', false );
	wp_enqueue_script( 'owl-carousel-js',  get_template_directory_uri().'/assets/js/owl.carousel.min.js', false );

	// Moby Styles
	wp_enqueue_style( 'moby-styles', get_template_directory_uri().'/assets/css/moby.min.css', false );
	// Moby Script
	wp_enqueue_script( 'moby-script',  get_template_directory_uri().'/assets/js/moby.min.js', false );

	// Bootstrap
	wp_enqueue_style( 'bootstrap-custom', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', false );
	wp_style_add_data( 'bootstrap-custom', 'rtl', 'replace' );

	// Custom Styles
	wp_enqueue_style( 'cfwd-custom-styles', get_template_directory_uri().'/assets/css/custom.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_style_add_data( 'cfwd-custom-styles', 'rtl', 'replace' );

}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );

/**
* Implement the Custom Header feature.
*/
require get_template_directory() . '/inc/custom-header.php';

/**
* Custom template tags for this theme.
*/
require get_template_directory() . '/inc/template-tags.php';

/**
* Functions which enhance the theme by hooking into WordPress.
*/
require get_template_directory() . '/inc/template-functions.php';

/**
* Customizer additions.
*/
require get_template_directory() . '/inc/customizer.php';

/**
* Load Jetpack compatibility file.
*/
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_filter('show_admin_bar', '__return_false');

// global options page for the multiple website panel content
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}
