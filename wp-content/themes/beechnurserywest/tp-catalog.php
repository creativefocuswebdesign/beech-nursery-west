<?php
/**
* Template Name: Catalog Template
*/

get_header();

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

?>

<?php

if($featured_img_url != "") { ?>
    <div class="custom-header-area" style="background-image:url('<?php echo $featured_img_url; ?>')" >
    <?php
} else {
    ?>
    <div class="custom-header-area" style="background-color:#60a511;">
    <?php
}

?>
    <div class="title"><h1><?php echo $post->post_title; ?></div>
</div>
<div class="catalog">
    <div class="container">
        <div class="table-responsive-lg">
            <div id="primary" class="content-area">
        		<main id="main" class="site-main text-center">
                    <p>Please fill out our contact form on the <b><u><a href="<?php echo get_site_url(); ?>/contact">Contact Us</a></u></b><br>page to request the password of the wholesale catalogue.</p>
            		<?php
            		while ( have_posts() ) :
            			the_post();

            			get_template_part( 'template-parts/content', 'page' );

            			// If comments are open or we have at least one comment, load up the comment template.
            			if ( comments_open() || get_comments_number() ) :
            				comments_template();
            			endif;

            		endwhile; // End of the loop.
            		?>

        		</main><!-- #main -->
        	</div><!-- #primary -->

        </div>
    </div>
</div>

<!--
<table class="table table-striped table-sm table-hover table-bordered">
    <tbody>
        <tr>
            <td>CATEGORY</td>
            <td scope="col">TYPE</td>
            <td scope="col">LATIN</td>
            <td scope="col">COMMON</td>
            <td scope="col">SIZE</td>
            <td scope="col">Price</td>
            <td scope="col">25+</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">ARCTOSTAPHYLOS</td>
            <td scope="col">Arctostaphylos uva-ursi</td>
            <td scope="col">Bearberry (Kinnikinick)</td>
            <td scope="col">1 GAL</td>
            <td scope="col">9.50</td>
            <td scope="col">9.15</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">BUXUS</td>
            <td scope="col">Buxus ‘Green Gem’</td>
            <td scope="col">Green Gem Boxwood</td>
            <td scope="col">3 GAL</td>
            <td scope="col">22.00</td>
            <td scope="col">19.50</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">BUXUS</td>
            <td scope="col">Buxus ‘Green Gem’</td>
            <td scope="col">Green Gem Boxwood</td>
            <td scope="col">5 GAL</td>
            <td scope="col">38.00</td>
            <td scope="col">35.00</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">BUXUS</td>
            <td scope="col">Buxus ‘Green Mound’</td>
            <td scope="col">Green Mound Boxwood</td>
            <td scope="col">3 GAL</td>
            <td scope="col">22.00</td>
            <td scope="col">19.50</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">BUXUS</td>
            <td scope="col">Buxus ‘Green Mound’</td>
            <td scope="col">Green Mound Boxwood</td>
            <td scope="col">5 GAL</td>
            <td scope="col">38.00</td>
            <td scope="col">35.00</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">BUXUS</td>
            <td scope="col">Buxus ‘Green Mountain’</td>
            <td scope="col">Green Mountain Boxwood</td>
            <td scope="col">3 GAL</td>
            <td scope="col">22.00</td>
            <td scope="col">19.50</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">BUXUS</td>
            <td scope="col">Buxus ‘Green Mountain’</td>
            <td scope="col">Green Mountain Boxwood</td>
            <td scope="col">5 GAL</td>
            <td scope="col">38.00</td>
            <td scope="col">35.00</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">BUXUS</td>
            <td scope="col">Buxus ‘Green Velvet’</td>
            <td scope="col">Green Velvet Boxwood</td>
            <td scope="col">3 GAL</td>
            <td scope="col">22.00</td>
            <td scope="col">19.50</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">BUXUS</td>
            <td scope="col">Buxus ‘Green Velvet’</td>
            <td scope="col">Green Velvet Boxwood</td>
            <td scope="col">5 GAL</td>
            <td scope="col">38.00</td>
            <td scope="col">35.00</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">COTONEASTER</td>
            <td scope="col">Cotoneaster dammeri</td>
            <td scope="col">Bearberry Cotoneaster</td>
            <td scope="col">1 GAL</td>
            <td scope="col">9.50</td>
            <td scope="col">8.75</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">COTONEASTER</td>
            <td scope="col">Cotoneaster dammeri</td>
            <td scope="col">Bearberry Cotoneaster</td>
            <td scope="col">2 GAL</td>
            <td scope="col">11.00</td>
            <td scope="col">9.50</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">COTONEASTER</td>
            <td scope="col">Cotoneaster dammeri ‘Coral Beauty’</td>
            <td scope="col">Coral Beauty Cotoneaster</td>
            <td scope="col">1 GAL</td>
            <td scope="col">8.25</td>
            <td scope="col">7.75</td>
        </tr>
        <tr>
            <td scope="col">BROADLEAF EVERGREENS</td>
            <td scope="col">COTONEASTER</td>
            <td scope="col">Cotoneaster dammeri ‘Coral Beauty’</td>
            <td scope="col">Coral Beauty Cotoneaster</td>
            <td scope="col">2 GAL</td>
            <td scope="col">11.00</td>
            <td scope="col">9.50</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus fortunei ‘Canadale Gold’</td>
            <td width="221">Canadale Gold Euonymus</td>
            <td width="79">2 GAL</td>
            <td width="71">12.75</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus fortunei ‘Coloratus’</td>
            <td width="221">Wintercreeper</td>
            <td width="79">1 GAL</td>
            <td width="71">7.75</td>
            <td width="69">6.50</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus fortunei ‘Coloratus’</td>
            <td width="221">Wintercreeper</td>
            <td width="79">2 GAL</td>
            <td width="71">13.00</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus fortunei ‘Emerald Gaiety’</td>
            <td width="221">Emerald Gaiety Euonymus</td>
            <td width="79">2 GAL</td>
            <td width="71">12.75</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus fortunei ‘Emerald ‘N Gold’</td>
            <td width="221">Emerald ‘N Gold Euonymus</td>
            <td width="79">2 GAL</td>
            <td width="71">12.75</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus fortunei ‘Goldtip’</td>
            <td width="221">Goldtip Euonymus</td>
            <td width="79">2 GAL</td>
            <td width="71">12.75</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus fortunei ‘Sarcoxie’</td>
            <td width="221">Sarcoxie Euonymus</td>
            <td width="79">2 GAL</td>
            <td width="71">12.75</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">ILEX</td>
            <td width="245">Ilex x meserveae ‘Blue Prince’</td>
            <td width="221">Blue Prince Holly</td>
            <td width="79">2 GAL</td>
            <td width="71">20.00</td>
            <td width="69">18.50</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">ILEX</td>
            <td width="245">Ilex x meserveae ‘Blue Princess’</td>
            <td width="221">Blue Princess Holly</td>
            <td width="79">2 GAL</td>
            <td width="71">20.00</td>
            <td width="69">18.50</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">PIERIS</td>
            <td width="245">Pieris Japonica ‘Mountain Fire’</td>
            <td width="221">Mountain Fire Pieris</td>
            <td width="79">2 GAL</td>
            <td width="71">21.00</td>
            <td width="69">19.50</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">RHODODENDRON</td>
            <td width="245">Rhododendron PJM</td>
            <td width="221">PJM Rhododendron</td>
            <td width="79">2 GAL</td>
            <td width="71">22.00</td>
            <td width="69">20.00</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">RHODODENDRON</td>
            <td width="245">Rhododendron PJM</td>
            <td width="221">PJM Rhododendron</td>
            <td width="79">3 GAL</td>
            <td width="71">38.00</td>
            <td width="69">35.00</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">RHODODENDRON</td>
            <td width="245">Rhododendron PJM</td>
            <td width="221">PJM Rhododendron</td>
            <td width="79">5 GAL</td>
            <td width="71">55.00</td>
            <td width="69">50.00</td>
        </tr>
        <tr>
            <td width="137">BROADLEAF EVERGREENS</td>
            <td width="181">YUCCA</td>
            <td width="245">Yucca filamentosa</td>
            <td width="221">Adam’s Needle</td>
            <td width="79">2 GAL</td>
            <td width="71">17.00</td>
            <td width="69">15.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis</td>
            <td width="221">White Cedar, nursery grown</td>
            <td width="79">125cm</td>
            <td width="71">45.00</td>
            <td width="69">40.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis</td>
            <td width="221">White Cedar, nursery grown</td>
            <td width="79">150cm</td>
            <td width="71">60.00</td>
            <td width="69">55.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis</td>
            <td width="221">White Cedar, nursery grown</td>
            <td width="79">175cm W.B.</td>
            <td width="71">110.00</td>
            <td width="69">95.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis</td>
            <td width="221">White Cedar, nursery grown</td>
            <td width="79">200cm W.B.</td>
            <td width="71">140.00</td>
            <td width="69">125.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis</td>
            <td width="221">White Cedar, nursery grown</td>
            <td width="79">250cm W.B.</td>
            <td width="71">190.00</td>
            <td width="69">170.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘DeGroots Spire’</td>
            <td width="221">DeGroots Spire Cedar</td>
            <td width="79">125cm</td>
            <td width="71">80.00</td>
            <td width="69">65.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘DeGroots Spire’</td>
            <td width="221">DeGroots Spire Cedar</td>
            <td width="79">150cm</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Little Giant’</td>
            <td width="221">Little Giant Cedar</td>
            <td width="79">3 GAL</td>
            <td width="71">19.50</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Little Giant’</td>
            <td width="221">Little Giant Cedar</td>
            <td width="79">5 GAL</td>
            <td width="71">30.00</td>
            <td width="69">28.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Nigra’</td>
            <td width="221">Black Cedar</td>
            <td width="79">125cm</td>
            <td width="71">45.00</td>
            <td width="69">40.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Nigra’</td>
            <td width="221">Black Cedar</td>
            <td width="79">150cm</td>
            <td width="71">60.00</td>
            <td width="69">55.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Nigra’</td>
            <td width="221">Black Cedar</td>
            <td width="79">175cm W.B.</td>
            <td width="71">110.00</td>
            <td width="69">95.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Nigra’</td>
            <td width="221">Black Cedar</td>
            <td width="79">200cm W.B.</td>
            <td width="71">140.00</td>
            <td width="69">125.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Nigra’</td>
            <td width="221">Black Cedar</td>
            <td width="79">250cm W.B.</td>
            <td width="71">190.00</td>
            <td width="69">170.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Smaragd’</td>
            <td width="221">Emeral Green Cedar</td>
            <td width="79">125cm</td>
            <td width="71">45.00</td>
            <td width="69">40.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Smaragd’</td>
            <td width="221">Emeral Green Cedar</td>
            <td width="79">150cm</td>
            <td width="71">60.00</td>
            <td width="69">55.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Smaragd’</td>
            <td width="221">Emeral Green Cedar</td>
            <td width="79">175cm</td>
            <td width="71">110.00</td>
            <td width="69">95.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Smaragd’</td>
            <td width="221">Emeral Green Cedar</td>
            <td width="79">200cm</td>
            <td width="71">140.00</td>
            <td width="69">125.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">CEDAR</td>
            <td width="245">Thuja Occidentalis ‘Smaragd’</td>
            <td width="221">Emeral Green Cedar</td>
            <td width="79">250cm</td>
            <td width="71">190.00</td>
            <td width="69">170.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">HEMLOCK</td>
            <td width="245">Tsuga Canadensis</td>
            <td width="221">Canadian Hemlock</td>
            <td width="79">125cm</td>
            <td width="71">110.00</td>
            <td width="69">95.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">HEMLOCK</td>
            <td width="245">Tsuga Canadensis</td>
            <td width="221">Canadian Hemlock</td>
            <td width="79">150cm</td>
            <td width="71">130.00</td>
            <td width="69">120.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">HEMLOCK</td>
            <td width="245">Tsuga Canadensis</td>
            <td width="221">Canadian Hemlock</td>
            <td width="79">175cm</td>
            <td width="71">160.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">HEMLOCK</td>
            <td width="245">Tsuga Canadensis</td>
            <td width="221">Canadian Hemlock</td>
            <td width="79">200cm</td>
            <td width="71">180.00</td>
            <td width="69">160.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">HEMLOCK</td>
            <td width="245">Tsuga Canadensis</td>
            <td width="221">Canadian Hemlock</td>
            <td width="79">250cm</td>
            <td width="71">300.00</td>
            <td width="69">280.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Abies Concolor</td>
            <td width="221">Silver Fir</td>
            <td width="79">150cm</td>
            <td width="71">120.00</td>
            <td width="69">115.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Abies Concolor</td>
            <td width="221">Silver Fir</td>
            <td width="79">175cm</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Abies Concolor</td>
            <td width="221">Silver Fir</td>
            <td width="79">200cm</td>
            <td width="71">170.00</td>
            <td width="69">160.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Abies Concolor</td>
            <td width="221">Silver Fir</td>
            <td width="79">250cm</td>
            <td width="71">300.00</td>
            <td width="69">280.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Chamaecyparis Nootkatensis Pendula</td>
            <td width="221">Weeping Nootka Cypress</td>
            <td width="79">125cm</td>
            <td width="71">55.00</td>
            <td width="69">45.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Chamaecyparis Nootkatensis Pendula</td>
            <td width="221">Weeping Nootka Cypress</td>
            <td width="79">150cm</td>
            <td width="71">125.00</td>
            <td width="69">120.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Chamaecyparis Nootkatensis Pendula</td>
            <td width="221">Weeping Nootka Cypress</td>
            <td width="79">175cm</td>
            <td width="71">140.00</td>
            <td width="69">130.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Chamaecyparis Nootkatensis Pendula</td>
            <td width="221">Weeping Nootka Cypress</td>
            <td width="79">200cm</td>
            <td width="71">165.00</td>
            <td width="69">155.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Chinensis ‘Fairview’</td>
            <td width="221">Fairview Juniper</td>
            <td width="79">150cm W.B.</td>
            <td width="71">75.00</td>
            <td width="69">65.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Chinensis ‘Fairview’</td>
            <td width="221">Fairview Juniper</td>
            <td width="79">175cm W.B.</td>
            <td width="71">105.00</td>
            <td width="69">100.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Chinensis ‘Fairview’</td>
            <td width="221">Fairview Juniper</td>
            <td width="79">200cm W.B.</td>
            <td width="71">140.00</td>
            <td width="69">130.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Chinensis ‘Monlep’</td>
            <td width="221">Mint Julep Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">16.88</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Horizontalis ‘Blue Chip’</td>
            <td width="221">Blue Chip Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Horizontalis ‘Plumosa Compacta’</td>
            <td width="221">Compact Andorra Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Horizontalis ‘Wiltoni’</td>
            <td width="221">Blue Rug Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Procumbens ‘Nana’</td>
            <td width="221">Japanese Garden Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">19.00</td>
            <td width="69">18.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Sabina</td>
            <td width="221">Savin Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Sabina ‘Arcadia’</td>
            <td width="221">Arcadia Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Sabina ‘Monna’</td>
            <td width="221">Calgary Carpet Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Sabina ‘Tamariscifolia’</td>
            <td width="221">Tamarix Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Squamata ‘Blue Star’</td>
            <td width="221">Blue star Juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">19.00</td>
            <td width="69">18.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Virginiana</td>
            <td width="221">Eastern Red Cedar</td>
            <td width="79">150cm W.B.</td>
            <td width="71">100.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Virginiana</td>
            <td width="221">Eastern Red Cedar</td>
            <td width="79">175cm W.B.</td>
            <td width="71">130.00</td>
            <td width="69">120.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Virginiana</td>
            <td width="221">Eastern Red Cedar</td>
            <td width="79">200cm W.B.</td>
            <td width="71">160.00</td>
            <td width="69">150.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Virginiana ‘Blue Arrow’</td>
            <td width="221">Blue Arrow Juniper</td>
            <td width="79">150cm W.B.</td>
            <td width="71">75.00</td>
            <td width="69">65.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Virginiana ‘Blue Arrow’</td>
            <td width="221">Blue Arrow Juniper</td>
            <td width="79">175cm W.B.</td>
            <td width="71">105.00</td>
            <td width="69">100.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus Virginiana ‘Blue Arrow’</td>
            <td width="221">Blue Arrow Juniper</td>
            <td width="79">200cm W.B.</td>
            <td width="71">140.00</td>
            <td width="69">130.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">JUNIPERUS</td>
            <td width="245">Juniperus X Pfitzeriana ‘Old Gold’</td>
            <td width="221">Old Gold juniper</td>
            <td width="79">3 GAL</td>
            <td width="71">18.00</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">LARCH</td>
            <td width="245">Larix Laricina</td>
            <td width="221">Canadian Tamarack</td>
            <td width="79">125cm</td>
            <td width="71">70.00</td>
            <td width="69">65.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">LARCH</td>
            <td width="245">Larix Laricina</td>
            <td width="221">Canadian Tamarack</td>
            <td width="79">150cm</td>
            <td width="71">105.00</td>
            <td width="69">100.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">LARCH</td>
            <td width="245">Larix Laricina</td>
            <td width="221">Canadian Tamarack</td>
            <td width="79">175cm</td>
            <td width="71">140.00</td>
            <td width="69">130.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">LARCH</td>
            <td width="245">Larix Laricina</td>
            <td width="221">Canadian Tamarack</td>
            <td width="79">200cm</td>
            <td width="71">175.00</td>
            <td width="69">160.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">LARCH</td>
            <td width="245">Microbiota Decussata</td>
            <td width="221">Siberian Cypress</td>
            <td width="79">3 GAL</td>
            <td width="71">20.00</td>
            <td width="69">19.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Mugo Var. Pumilio</td>
            <td width="221">Mugho Pine</td>
            <td width="79">3 GAL</td>
            <td width="71">20.00</td>
            <td width="69">18.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Nigra</td>
            <td width="221">Austrian Pine</td>
            <td width="79">125cm</td>
            <td width="71">100.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Nigra</td>
            <td width="221">Austrian Pine</td>
            <td width="79">150cm</td>
            <td width="71">115.00</td>
            <td width="69">105.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Nigra</td>
            <td width="221">Austrian Pine</td>
            <td width="79">175cm</td>
            <td width="71">150.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Nigra</td>
            <td width="221">Austrian Pine</td>
            <td width="79">200cm</td>
            <td width="71">160.00</td>
            <td width="69">150.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Nigra</td>
            <td width="221">Austrian Pine</td>
            <td width="79">250cm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Strobus</td>
            <td width="221">White Pine</td>
            <td width="79">125cm</td>
            <td width="71">100.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Strobus</td>
            <td width="221">White Pine</td>
            <td width="79">150cm</td>
            <td width="71">115.00</td>
            <td width="69">105.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Strobus</td>
            <td width="221">White Pine</td>
            <td width="79">175cm</td>
            <td width="71">150.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Strobus</td>
            <td width="221">White Pine</td>
            <td width="79">200cm</td>
            <td width="71">160.00</td>
            <td width="69">150.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Strobus</td>
            <td width="221">White Pine</td>
            <td width="79">250cm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Sylvestris</td>
            <td width="221">Scot’s Pine</td>
            <td width="79">125cm</td>
            <td width="71">100.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Sylvestris</td>
            <td width="221">Scot’s Pine</td>
            <td width="79">150cm</td>
            <td width="71">115.00</td>
            <td width="69">105.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Sylvestris</td>
            <td width="221">Scot’s Pine</td>
            <td width="79">175cm</td>
            <td width="71">150.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Sylvestris</td>
            <td width="221">Scot’s Pine</td>
            <td width="79">200cm</td>
            <td width="71">160.00</td>
            <td width="69">150.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">PINE</td>
            <td width="245">Pinus Sylvestris</td>
            <td width="221">Scot’s Pine</td>
            <td width="79">250cm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Abies</td>
            <td width="221">Norway Spruce</td>
            <td width="79">150cm</td>
            <td width="71">115.00</td>
            <td width="69">105.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Abies</td>
            <td width="221">Norway Spruce</td>
            <td width="79">175cm</td>
            <td width="71">145.00</td>
            <td width="69">130.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Abies</td>
            <td width="221">Norway Spruce</td>
            <td width="79">200cm</td>
            <td width="71">160.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Abies</td>
            <td width="221">Norway Spruce</td>
            <td width="79">250cm</td>
            <td width="71">280.00</td>
            <td width="69">260.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Abies ‘Nidiformis’</td>
            <td width="221">Dwarf Nest Spruce</td>
            <td width="79">3 GAL</td>
            <td width="71">22.00</td>
            <td width="69">20.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Glauca</td>
            <td width="221">White Spruce</td>
            <td width="79">125cm</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Glauca</td>
            <td width="221">White Spruce</td>
            <td width="79">150cm</td>
            <td width="71">120.00</td>
            <td width="69">110.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Glauca</td>
            <td width="221">White Spruce</td>
            <td width="79">175cm</td>
            <td width="71">155.00</td>
            <td width="69">140.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Glauca</td>
            <td width="221">White Spruce</td>
            <td width="79">200cm</td>
            <td width="71">180.00</td>
            <td width="69">155.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Glauca</td>
            <td width="221">White Spruce</td>
            <td width="79">250cm</td>
            <td width="71">290.00</td>
            <td width="69">265.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Glauca ‘Conica’</td>
            <td width="221">Dwarf Alberta White Spruce</td>
            <td width="79">3 GAL</td>
            <td width="71">25.00</td>
            <td width="69">22.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Omorika</td>
            <td width="221">Serbian Spruce</td>
            <td width="79">150cm</td>
            <td width="71">120.00</td>
            <td width="69">115.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Omorika</td>
            <td width="221">Serbian Spruce</td>
            <td width="79">175cm</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Omorika</td>
            <td width="221">Serbian Spruce</td>
            <td width="79">200cm</td>
            <td width="71">170.00</td>
            <td width="69">160.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Omorika</td>
            <td width="221">Serbian Spruce</td>
            <td width="79">250cm</td>
            <td width="71">300.00</td>
            <td width="69">280.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens</td>
            <td width="221">Colorado Spruce</td>
            <td width="79">125cm</td>
            <td width="71">100.00</td>
            <td width="69">90.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens</td>
            <td width="221">Colorado Spruce</td>
            <td width="79">150cm</td>
            <td width="71">120.00</td>
            <td width="69">115.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens</td>
            <td width="221">Colorado Spruce</td>
            <td width="79">175cm</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens</td>
            <td width="221">Colorado Spruce</td>
            <td width="79">200cm</td>
            <td width="71">170.00</td>
            <td width="69">160.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens</td>
            <td width="221">Colorado Spruce</td>
            <td width="79">250cm</td>
            <td width="71">300.00</td>
            <td width="69">280.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Fat Albert’</td>
            <td width="221">Fat Albert Spruce</td>
            <td width="79">150cm</td>
            <td width="71">130.00</td>
            <td width="69">120.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Fat Albert’</td>
            <td width="221">Fat Albert Spruce</td>
            <td width="79">175cm</td>
            <td width="71">175.00</td>
            <td width="69">160.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Fat Albert’</td>
            <td width="221">Fat Albert Spruce</td>
            <td width="79">200cm</td>
            <td width="71">190.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Glauca’</td>
            <td width="221">Blue Colorado Spruce</td>
            <td width="79">125cm</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Glauca’</td>
            <td width="221">Blue Colorado Spruce</td>
            <td width="79">150cm</td>
            <td width="71">120.00</td>
            <td width="69">110.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Glauca’</td>
            <td width="221">Blue Colorado Spruce</td>
            <td width="79">175cm</td>
            <td width="71">155.00</td>
            <td width="69">140.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Glauca’</td>
            <td width="221">Blue Colorado Spruce</td>
            <td width="79">200cm</td>
            <td width="71">180.00</td>
            <td width="69">155.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Glauca’</td>
            <td width="221">Blue Colorado Spruce</td>
            <td width="79">250cm</td>
            <td width="71">290.00</td>
            <td width="69">265.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Glauca Globosa’</td>
            <td width="221">Globe Blue Spruce</td>
            <td width="79">5 GAL</td>
            <td width="71">85.00</td>
            <td width="69">75.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Glauca Globosa’ – Standard</td>
            <td width="221">Globe Blue Spruce Standard</td>
            <td width="79">10 GAL</td>
            <td width="71">135.00</td>
            <td width="69">125.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens Hoopsii</td>
            <td width="221">Hoopsi Blue Spruce</td>
            <td width="79">150cm</td>
            <td width="71">200.00</td>
            <td width="69">180.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens Hoopsii</td>
            <td width="221">Hoopsi Blue Spruce</td>
            <td width="79">175cm</td>
            <td width="71">220.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens Hoopsii</td>
            <td width="221">Hoopsi Blue Spruce</td>
            <td width="79">200cm</td>
            <td width="71">255.00</td>
            <td width="69">240.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Iseli Fastigiate’</td>
            <td width="221">Iseli Fastigiate Colorado Blue Spruce</td>
            <td width="79">150cm</td>
            <td width="71">160.00</td>
            <td width="69">150.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Iseli Fastigiate’</td>
            <td width="221">Iseli Fastigiate Colorado Blue Spruce</td>
            <td width="79">175cm</td>
            <td width="71">190.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">SPRUCE</td>
            <td width="245">Picea Pungens ‘Iseli Fastigiate’</td>
            <td width="221">Iseli Fastigiate Colorado Blue Spruce</td>
            <td width="79">200cm</td>
            <td width="71">220.00</td>
            <td width="69">200.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus&nbsp; Media Wardii</td>
            <td width="221">Ward’s Yew</td>
            <td width="79">3 GAL</td>
            <td width="71">22.50</td>
            <td width="69">20.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Cuspidata Capitata</td>
            <td width="221">Clipped Cone Yew</td>
            <td width="79">100cm</td>
            <td width="71">120.00</td>
            <td width="69">105.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Cuspidata Capitata</td>
            <td width="221">Clipped Cone Yew</td>
            <td width="79">80cm</td>
            <td width="71">85.00</td>
            <td width="69">75.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Cuspidata ‘Nana’</td>
            <td width="221">Dwarf Japanese Yew</td>
            <td width="79">3 GAL</td>
            <td width="71">27.00</td>
            <td width="69">25.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Media ‘Densiforms’</td>
            <td width="221">Dense Yew</td>
            <td width="79">3 GAL</td>
            <td width="71">22.50</td>
            <td width="69">20.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Media ‘Densiforms’</td>
            <td width="221">Dense Yew</td>
            <td width="79">5 GAL</td>
            <td width="71">35.00</td>
            <td width="69">30.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Media ‘Hicksii’</td>
            <td width="221">Hick’s Yew</td>
            <td width="79">100cm</td>
            <td width="71">100.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Media ‘Hicksii’</td>
            <td width="221">Hick’s Yew</td>
            <td width="79">3 GAL</td>
            <td width="71">22.50</td>
            <td width="69">20.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Media ‘Hicksii’</td>
            <td width="221">Hick’s Yew</td>
            <td width="79">5 GAL</td>
            <td width="71">40.00</td>
            <td width="69">35.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Media ‘Hillii’</td>
            <td width="221">Hill’s Yew</td>
            <td width="79">100cm B.B.</td>
            <td width="71">100.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Media ‘Hillii’</td>
            <td width="221">Hill’s Yew</td>
            <td width="79">3 GAL</td>
            <td width="71">22.50</td>
            <td width="69">20.50</td>
        </tr>
        <tr>
            <td width="137">EVERGREENS</td>
            <td width="181">YEW</td>
            <td width="245">Taxus Media ‘Hillii’</td>
            <td width="221">Hill’s Yew</td>
            <td width="79">5 GAL</td>
            <td width="71">40.00</td>
            <td width="69">35.00</td>
        </tr>
        <tr>
            <td width="137">HARDY FERNS</td>
            <td width="181">ATHYRIUM</td>
            <td width="245">Athyrium Filix-Femina</td>
            <td width="221">Lady Fern</td>
            <td width="79">1 GAL</td>
            <td width="71">7.40</td>
            <td width="69">7.20</td>
        </tr>
        <tr>
            <td width="137">HARDY FERNS</td>
            <td width="181">ATHYRIUM</td>
            <td width="245">Athyrium Niponicum ‘Pictum’</td>
            <td width="221">Japanese Painted Fern</td>
            <td width="79">1 GAL</td>
            <td width="71">7.40</td>
            <td width="69">7.20</td>
        </tr>
        <tr>
            <td width="137">HARDY FERNS</td>
            <td width="181">MATTEUCCIA</td>
            <td width="245">Matteuccia Struthiopteris</td>
            <td width="221">Ostrich Fern</td>
            <td width="79">1 GAL</td>
            <td width="71">7.40</td>
            <td width="69">7.20</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">ANDROPOGON</td>
            <td width="245">Andropogon Gerardii</td>
            <td width="221">Bigblue Stem/Turkey Foot Bluestem</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">BOUTELOUA</td>
            <td width="245">Bouteloua Gracilis</td>
            <td width="221">Blue Grana/Mosquito Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">CALAMAGROSTIS</td>
            <td width="245">Calamagrostis x Acutiflora ‘Karl Foerster’/’Stricta’</td>
            <td width="221">Feather Reed Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">CALAMAGROSTIS</td>
            <td width="245">Calamagrostis x Acutiflora ‘Karl Foerster’/’Stricta’</td>
            <td width="221">Feather Reed Grass</td>
            <td width="79">2 GAL</td>
            <td width="71">9.75</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">CAREX</td>
            <td width="245">Carex Morrowii ‘Ice Dance’</td>
            <td width="221">Ice Dance Sedge</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">CAREX</td>
            <td width="245">Carex Pensylvanica</td>
            <td width="221">Pennsylvania Sedge</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">FESTUCA</td>
            <td width="245">Festuca Glauca ‘Elijah Blue’</td>
            <td width="221">Elijah Blue Fescue</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.30</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">HAKONECHLOA</td>
            <td width="245">Hakonechloa Macra ‘All Gold’</td>
            <td width="221">Golden Japanese Forest Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">7.75</td>
            <td width="69">7.25</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">HELICTOTRICHON</td>
            <td width="245">Helictotrichon Sempervirens ‘Saphirsprudel’</td>
            <td width="221">Sapphire Fountain Blue Oat Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.30</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">IMPERATA</td>
            <td width="245">Imperata Cylindrica ‘Red Baron’/’Rubra’</td>
            <td width="221">Japanese Blood Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">MISCANTHUS</td>
            <td width="245">Miscanthus Sinensis ‘Gracillimus’</td>
            <td width="221">Maiden Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">MISCANTHUS</td>
            <td width="245">Miscanthus Sinensis ‘Gracillimus’</td>
            <td width="221">Maiden Grass</td>
            <td width="79">2 GAL</td>
            <td width="71">10.00</td>
            <td width="69">9.75</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">MISCANTHUS</td>
            <td width="245">Miscanthus Sinensis ‘Strictus’</td>
            <td width="221">Porcupine Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">7.00</td>
            <td width="69">6.85</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">MISCANTHUS</td>
            <td width="245">Miscanthus Sinensis ‘Zebrinus’</td>
            <td width="221">Zebra Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">7.00</td>
            <td width="69">6.85</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">PANICUM</td>
            <td width="245">Panicum Virgatum ‘Heavy Metal’</td>
            <td width="221">Heavy Metal Switch Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">PANICUM</td>
            <td width="245">Panicum Virgatum ‘Shenandoah’</td>
            <td width="221">Shenandoah Switch Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">PANICUM</td>
            <td width="245">Panicum Virgatum Var. Virgatum</td>
            <td width="221">Switch Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">PENNISETUM</td>
            <td width="245">Pennisetum Alopecuroides</td>
            <td width="221">Fountain Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">PENNISETUM</td>
            <td width="245">Pennisetum Alopecuroides</td>
            <td width="221">Fountain Grass</td>
            <td width="79">2 GAL</td>
            <td width="71">10.00</td>
            <td width="69">9.75</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">PENNISETUM</td>
            <td width="245">Pennisetum Alopecuroides ‘Hamelin’</td>
            <td width="221">Dwarf Fountain Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">PHALARIS</td>
            <td width="245">Phalaris Arundinacea ‘Picta’</td>
            <td width="221">Ribbon Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.30</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">SCHIZACHYRIUM</td>
            <td width="245">Schizachyrium Scoparium/Andropogon Scoparius</td>
            <td width="221">Little Bluestem</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">SORGHASTRUM</td>
            <td width="245">Sorghastrum Nutans</td>
            <td width="221">Indian Grass</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">ORNAMENTAL GRASSES</td>
            <td width="181">SPOROBOLUS</td>
            <td width="245">Sporobolus Heterolepsis</td>
            <td width="221">Prairie Dropseed</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.35</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ACHILLEA</td>
            <td width="245">Achillea x ‘Moonshine’</td>
            <td width="221">Moonshine Yarrow</td>
            <td width="79">1 GAL</td>
            <td width="71">6.10</td>
            <td width="69">5.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">AJUGA</td>
            <td width="245">Ajuga Reptans ‘Burgundy Glow’</td>
            <td width="221">Burgundy Glow Bugleweed</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.80</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">AJUGA</td>
            <td width="245">Ajuga Tenorii ‘Valfredda’</td>
            <td width="221">Chocolate Chip Bugleweed</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.80</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ALLIUM</td>
            <td width="245">Allium ‘Millenium</td>
            <td width="221">Millenium Allium</td>
            <td width="79">1 GAL</td>
            <td width="71">7.00</td>
            <td width="69">6.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ANEMONE</td>
            <td width="245">Anemone x Hybrida ‘Honorine Jobert’</td>
            <td width="221">Windflower</td>
            <td width="79">1 GAL</td>
            <td width="71">6.60</td>
            <td width="69">6.55</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ARTEMISIA</td>
            <td width="245">Artemisia Schmidtiana ‘Silvermound’/’Nana’</td>
            <td width="221">Silver Mound Wormwood</td>
            <td width="79">1 GAL</td>
            <td width="71">5.90</td>
            <td width="69">5.70</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ASCLEPIAS</td>
            <td width="245">Asclepias Tuberosa</td>
            <td width="221">Butterfly Milkweed</td>
            <td width="79">1 GAL</td>
            <td width="71">6.25</td>
            <td width="69">6.10</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ASTER</td>
            <td width="245">Aster Dumosus ‘Wood’s Blue’</td>
            <td width="221">Blue Wood Aster</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ASTER</td>
            <td width="245">Aster Dumosus ‘Wood’s Pink’</td>
            <td width="221">Pink Wood Aster</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ASTER</td>
            <td width="245">Aster Dumosus ‘Wood’s Purple’</td>
            <td width="221">Purple Wood Aster</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ASTILBE</td>
            <td width="245">Astilbe Arendsii ‘Bridal Veil’</td>
            <td width="221">Bridal Veil Astilbe</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ASTILBE</td>
            <td width="245">Astilbe Arendsii ‘Fanal’</td>
            <td width="221">Bridal Veil Astilbe</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ASTILBE</td>
            <td width="245">Astilbe Chinensis ‘Pumila’</td>
            <td width="221">Chinese Astilbe</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ASTILBE</td>
            <td width="245">Astilbe Japonica ‘Montgomery’</td>
            <td width="221">Montgomery Astilbe</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">BAPTISIA</td>
            <td width="245">Baptisia Australis</td>
            <td width="221">Blue False Indigo</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">BRUNNERA</td>
            <td width="245">Brunnera Macrophylla ‘Jack Frost’</td>
            <td width="221">Jack Frost Brunnera</td>
            <td width="79">1 GAL</td>
            <td width="71">12.00</td>
            <td width="69">11.55</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">CAMPANULA</td>
            <td width="245">Campanula Carpatica ‘Blue Clips’</td>
            <td width="221">Carpathian Bellflower</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">CAMPANULA</td>
            <td width="245">Campanula Carpatica ‘White Clips’</td>
            <td width="221">Carpathian Bellflower</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">CHELONE</td>
            <td width="245">Chelone Glabra</td>
            <td width="221">White Turtlehead</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">CHELONE</td>
            <td width="245">Chelone Obliqua</td>
            <td width="221">Rose Turtlehead</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">CONVALLARIA</td>
            <td width="245">Convallaria Majalis</td>
            <td width="221">Lily of the Valley</td>
            <td width="79">1 GAL</td>
            <td width="71">6.30</td>
            <td width="69">6.15</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">COREOPSIS</td>
            <td width="245">Coreopsis Grandiflora ‘Rising Sun’</td>
            <td width="221">Rising Sun Tickseed</td>
            <td width="79">1 GAL</td>
            <td width="71">7.15</td>
            <td width="69">6.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">COREOPSIS</td>
            <td width="245">Coreopsis Verticillata ‘ Zagreb’</td>
            <td width="221">Zagreb Tickseed</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">COREOPSIS</td>
            <td width="245">Coreopsis Verticillata ‘Moonbeam’</td>
            <td width="221">Moonbeam Threadleaf Tickseed</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">DELPHINIUM</td>
            <td width="245">Delphinium Elatum ‘Blue Lace’</td>
            <td width="221">Blue Lace Delphinium</td>
            <td width="79">1 GAL</td>
            <td width="71">7.10</td>
            <td width="69">6.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">DELPHINIUM</td>
            <td width="245">Delphinium Elatum ‘Blushing Brides’</td>
            <td width="221">Delphinium-Hybrid Bee</td>
            <td width="79">1 GAL</td>
            <td width="71">7.10</td>
            <td width="69">6.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">DIANTHUS</td>
            <td width="245">Dianthus Gratianopoltanus ‘Fire Witch’</td>
            <td width="221">Fire Witch Cheddar Pink</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">DICENTRA</td>
            <td width="245">Dicentra Spectabilis ‘ Valentine’</td>
            <td width="221">Red Bleeding Heart</td>
            <td width="79">1 GAL</td>
            <td width="71">7.00</td>
            <td width="69">6.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">DICENTRA</td>
            <td width="245">Dicentra Spectabilis ‘Alba’</td>
            <td width="221">White Bleeding Heart</td>
            <td width="79">1 GAL</td>
            <td width="71">7.00</td>
            <td width="69">6.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ECHINACEA</td>
            <td width="245">Echinacea Purpurea</td>
            <td width="221">Purple Coneflower</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ECHINACEA</td>
            <td width="245">Echinacea Purpurea ‘ Green Envy’</td>
            <td width="221">Green Envy Coneflower</td>
            <td width="79">1 GAL</td>
            <td width="71">10.50</td>
            <td width="69">10.25</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ECHINACEA</td>
            <td width="245">Echinacea Purpurea ‘Kim’s Knee High’</td>
            <td width="221">Kim’s Knee High Coneflower</td>
            <td width="79">1 GAL</td>
            <td width="71">10.50</td>
            <td width="69">10.25</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ECHINACEA</td>
            <td width="245">Echinacea Purpurea ‘Magnus’</td>
            <td width="221">Magnus Coneflower</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ECHINACEA</td>
            <td width="245">Echinacea Purpurea ‘White Swan’</td>
            <td width="221">White Swan Coneflower</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ECHINACEA</td>
            <td width="245">Geranium Psilostemon ‘Rozanne’</td>
            <td width="221">Rozanne Cranesbill</td>
            <td width="79">1 GAL</td>
            <td width="71">9.00</td>
            <td width="69">8.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">ECHINACEA</td>
            <td width="245">Geranium x ‘Johnson’s Blue’</td>
            <td width="221">Johnson’s Blue Cranesbill</td>
            <td width="79">1 GAL</td>
            <td width="71">7.00</td>
            <td width="69">6.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEMEROCALLIS</td>
            <td width="245">Hemerocallis&nbsp; ‘Stella D’Oro’</td>
            <td width="221">Daylily ‘Stella D’Oro’</td>
            <td width="79">1 GAL</td>
            <td width="71">5.25</td>
            <td width="69">5.15</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEMEROCALLIS</td>
            <td width="245">Hemerocallis&nbsp; ‘Stella D’Oro’</td>
            <td width="221">Daylily ‘Stella D’Oro’</td>
            <td width="79">2 GAL</td>
            <td width="71">9.00</td>
            <td width="69">8.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEMEROCALLIS</td>
            <td width="245">Hemerocallis ‘Happy Returns’</td>
            <td width="221">Daylily ‘Happy Returns’</td>
            <td width="79">1 GAL</td>
            <td width="71">5.75</td>
            <td width="69">5.55</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEMEROCALLIS</td>
            <td width="245">Hemerocallis ‘Hyperion’</td>
            <td width="221">Daylily&nbsp; ‘Hyperion’</td>
            <td width="79">1 GAL</td>
            <td width="71">5.75</td>
            <td width="69">5.55</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEMEROCALLIS</td>
            <td width="245">Hemerocallis ‘Joan Senior’</td>
            <td width="221">Daylily ‘Joan Senior’</td>
            <td width="79">1 GAL</td>
            <td width="71">5.75</td>
            <td width="69">5.55</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEMEROCALLIS</td>
            <td width="245">Hemerocallis ‘Pardon Me’</td>
            <td width="221">Daylily ‘Pardon Me’</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEUCHERA</td>
            <td width="245">HEUCHERA ‘ Lime Marmalade’</td>
            <td width="221">Lime Marmalade Coral Bells</td>
            <td width="79">1 GAL</td>
            <td width="71">11.15</td>
            <td width="69">10.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEUCHERA</td>
            <td width="245">Heuchera ‘Obsidian’</td>
            <td width="221">Obsidian Coral Bells</td>
            <td width="79">1 GAL</td>
            <td width="71">11.15</td>
            <td width="69">10.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HEUCHERA</td>
            <td width="245">Heuchera ‘Plum Pudding’ (Americana)</td>
            <td width="221">Plum Pudding Coral Bells</td>
            <td width="79">1 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.25</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘August Moon’</td>
            <td width="221">Plantain Lily ‘August Moon’</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘Big Daddy’</td>
            <td width="221">Plantain Lily&nbsp; ‘Big Daddy’</td>
            <td width="79">1 GAL</td>
            <td width="71">8.05</td>
            <td width="69">7.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘Francee’</td>
            <td width="221">Plantain Lily ‘Francee’</td>
            <td width="79">1 GAL</td>
            <td width="71">6.10</td>
            <td width="69">5.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘Halcyon’</td>
            <td width="221">Plantain Lily ‘Halcyon’</td>
            <td width="79">1 GAL</td>
            <td width="71">7.75</td>
            <td width="69">7.45</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘June’</td>
            <td width="221">Plantain Lily ‘June’</td>
            <td width="79">1 GAL</td>
            <td width="71">7.75</td>
            <td width="69">7.45</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘Minuteman’</td>
            <td width="221">Plantain Lily ‘Minuteman’</td>
            <td width="79">1 GAL</td>
            <td width="71">7.75</td>
            <td width="69">7.45</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘Patriot’</td>
            <td width="221">Plantain Lily ‘Patriot’</td>
            <td width="79">1 GAL</td>
            <td width="71">7.75</td>
            <td width="69">7.45</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘Royal Standard’</td>
            <td width="221">Plantain Lily ‘Royal Standard’</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘Royal Standard’</td>
            <td width="221">Plantain Lily ‘Royal Standard’</td>
            <td width="79">2 GAL</td>
            <td width="71">9.85</td>
            <td width="69">9.45</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">HOSTA</td>
            <td width="245">Hosta ‘Sum &amp; Substance’</td>
            <td width="221">Plantain Lily ‘Sum &amp; Substance’</td>
            <td width="79">1 GAL</td>
            <td width="71">7.75</td>
            <td width="69">7.45</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">IRIS</td>
            <td width="245">Iris Pallida Argentea Variegata</td>
            <td width="221">Silver-variegata Sweet Iris</td>
            <td width="79">1 GAL</td>
            <td width="71">10.75</td>
            <td width="69">10.35</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">IRIS</td>
            <td width="245">Iris Sibirica ‘Caesar’s Brother’</td>
            <td width="221">Siberian Iris ‘Caesar’s Brother’</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">IRIS</td>
            <td width="245">Iris Versicolor</td>
            <td width="221">Blue Flag Iris</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">LAVANDULA</td>
            <td width="245">Lavandula Angustifolia ‘Hidcote Blue’</td>
            <td width="221">English Lavender</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.80</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">LAVANDULA</td>
            <td width="245">Lavandula Angustifolia ‘Munstead’</td>
            <td width="221">Old English Lavender</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.80</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">LEUCANTHEMUM</td>
            <td width="245">Leucanthemum x Superbum ‘Becky’</td>
            <td width="221">Becky Shasta Daisy</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">LIATRIS</td>
            <td width="245">Liatris Spicta ‘Kobold’</td>
            <td width="221">Kobold Blazing Star / Gayfeather</td>
            <td width="79">1 GAL</td>
            <td width="71">6.25</td>
            <td width="69">6.10</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">LIRIOPE</td>
            <td width="245">Liriope Muscari ‘Big Blue’</td>
            <td width="221">Lily-turf Blue</td>
            <td width="79">1 GAL</td>
            <td width="71">7.00</td>
            <td width="69">6.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">MONARDA</td>
            <td width="245">Monarda Fistulosa</td>
            <td width="221">Wild Bee Balm</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">PEROVSKIA</td>
            <td width="245">Perovskia Artiplicifolia ‘Little Spire’</td>
            <td width="221">Little Spire Russian Sage</td>
            <td width="79">1 GAL</td>
            <td width="71">6.55</td>
            <td width="69">6.30</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">PHLOX</td>
            <td width="245">Phlox Paniculata</td>
            <td width="221">Summer Phlox</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">PHLOX</td>
            <td width="245">Phlox Subulata&nbsp; ‘Candy Stripe’</td>
            <td width="221">Summer Phlox ‘Candy Stripe’</td>
            <td width="79">1 GAL</td>
            <td width="71">6.10</td>
            <td width="69">5.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">PHLOX</td>
            <td width="245">Phlox Subulata ‘Emerald Blue’</td>
            <td width="221">Summer Phlox ‘Emerald Blue’</td>
            <td width="79">1 GAL</td>
            <td width="71">6.10</td>
            <td width="69">5.85</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">RUDBECKIA</td>
            <td width="245">Rudbeckia Fulgida ‘Goldstrum’</td>
            <td width="221">Black-eyed Susan</td>
            <td width="79">1 GAL</td>
            <td width="71">5.80</td>
            <td width="69">5.60</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">RUDBECKIA</td>
            <td width="245">Rudbeckia Hirta</td>
            <td width="221">Gloriosa Daisy</td>
            <td width="79">1 GAL</td>
            <td width="71">5.95</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">SALVIA</td>
            <td width="245">Salvia Nemorosa ‘Haeumanarc’</td>
            <td width="221">Marcus Salvia</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">SALVIA</td>
            <td width="245">Salvia Nemorosa ‘Pink Friesland’</td>
            <td width="221">Perennial Sage</td>
            <td width="79">1 GAL</td>
            <td width="71">6.20</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">SCABIOSA</td>
            <td width="245">Scabiosa Columbaria ‘Butterfly Blue’</td>
            <td width="221">Pincushion Flower</td>
            <td width="79">1 GAL</td>
            <td width="71">6.85</td>
            <td width="69">6.55</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">SEDUM</td>
            <td width="245">Sedum Spectabile ‘Autumn Fire’</td>
            <td width="221">Autumn Fire Stonecrop</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">SEDUM</td>
            <td width="245">Sedum Spurium ‘Dragon’s Blood’</td>
            <td width="221">Dragon’s Blood Stonecrop</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">SEDUM</td>
            <td width="245">Sedum x ‘Autumn Joy’</td>
            <td width="221">Autumn Joy Stonecrop</td>
            <td width="79">1 GAL</td>
            <td width="71">6.00</td>
            <td width="69">5.75</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">THYMUS</td>
            <td width="245">Thymus Pseudolanuginosus</td>
            <td width="221">Woolly Thyme</td>
            <td width="79">1 GAL</td>
            <td width="71">6.25</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">THYMUS</td>
            <td width="245">Thymus Serpyllum/Praecox ‘Coccineus’</td>
            <td width="221">Red Creeping Thyme</td>
            <td width="79">1 GAL</td>
            <td width="71">6.25</td>
            <td width="69">6.00</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">TIARELLA</td>
            <td width="245">Tiarella ‘Iron Butterfly’</td>
            <td width="221">Iron Butterfly Foamflower</td>
            <td width="79">1 GAL</td>
            <td width="71">9.00</td>
            <td width="69">8.55</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">VERONICA</td>
            <td width="245">Veronica Spicata ‘Royal Candles’</td>
            <td width="221">Royal Candles Speedwell</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">VERONICA</td>
            <td width="245">Veronica ‘Sunny Border Blue’</td>
            <td width="221">Border Blue Speedwell</td>
            <td width="79">1 GAL</td>
            <td width="71">6.75</td>
            <td width="69">6.30</td>
        </tr>
        <tr>
            <td width="137">PERENNIALS</td>
            <td width="181">VINCA</td>
            <td width="245">Vinca Minor</td>
            <td width="221">Perwinkle/Myrtle</td>
            <td width="79">1 GAL</td>
            <td width="71">6.15</td>
            <td width="69">5.95</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Ginnala ‘Flame’</td>
            <td width="221">Flame Maple</td>
            <td width="79">150cm</td>
            <td width="71">120.00</td>
            <td width="69">110.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Ginnala ‘Flame’</td>
            <td width="221">Flame Maple</td>
            <td width="79">3 GAL</td>
            <td width="71">12.75</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum ‘Bloodgood’</td>
            <td width="221">Bloodgood Japanese Maple</td>
            <td width="79">10 GAL</td>
            <td width="71">145.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum ‘Bloodgood’</td>
            <td width="221">Bloodgood Japanese Maple</td>
            <td width="79">15 GAL</td>
            <td width="71">165.00</td>
            <td width="69">155.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum ‘Bloodgood’</td>
            <td width="221">Bloodgood Japanese Maple</td>
            <td width="79">50mm W.B. Specimen</td>
            <td width="71">275.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum Dissectum ‘Crimson Queen’</td>
            <td width="221">Crimson Queen Japanese Maple</td>
            <td width="79">10 GAL</td>
            <td width="71">155.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum Dissectum ‘Crimson Queen’</td>
            <td width="221">Crimson Queen Japanese Maple</td>
            <td width="79">50mm W.B. Specimen</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum Dissectum ‘Inaba Shidare’</td>
            <td width="221">Inabe Shidare Weeping Japanese Maple</td>
            <td width="79">10 GAL</td>
            <td width="71">155.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum Dissectum ‘Inaba Shidare’</td>
            <td width="221">Inabe Shidare Weeping Japanese Maple</td>
            <td width="79">50mm W.B. Specimen</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum Dissectum ‘Tamukeyama’</td>
            <td width="221">Cutleaf Japanese Maple</td>
            <td width="79">10 GAL</td>
            <td width="71">155.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum ‘Sango Kaku’</td>
            <td width="221">Coral Bark Japanese Maple</td>
            <td width="79">10 GAL</td>
            <td width="71">155.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ACER</td>
            <td width="245">Acer Palmatum ‘Waterfall’</td>
            <td width="221">Waterfall Japanese Maple</td>
            <td width="79">10 GAL</td>
            <td width="71">155.00</td>
            <td width="69">135.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">AMELANCHIER</td>
            <td width="245">Amelanchier Canadensis</td>
            <td width="221">Serviceberry/Shadblow</td>
            <td width="79">150cm W.B.</td>
            <td width="71">120.00</td>
            <td width="69">110.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">AMELANCHIER</td>
            <td width="245">Amelanchier Canadensis</td>
            <td width="221">Serviceberry/Shadblow</td>
            <td width="79">175cm W.B.</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">AMELANCHIER</td>
            <td width="245">Amelanchier Canadensis</td>
            <td width="221">Serviceberry/Shadblow</td>
            <td width="79">2 GAL</td>
            <td width="71">10.00</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">AMELANCHIER</td>
            <td width="245">Amelanchier Canadensis</td>
            <td width="221">Serviceberry/Shadblow</td>
            <td width="79">200cm W.B.</td>
            <td width="71">185.00</td>
            <td width="69">170.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">AMELANCHIER</td>
            <td width="245">Amelanchier Canadensis</td>
            <td width="221">Serviceberry/Shadblow</td>
            <td width="79">250cm W.B.</td>
            <td width="71">255.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">AMELANCHIER</td>
            <td width="245">Amelanchier Canadensis</td>
            <td width="221">Serviceberry/Shadblow</td>
            <td width="79">3 GAL</td>
            <td width="71">16.50</td>
            <td width="69">15.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ARONIA</td>
            <td width="245">Aronia Arbutifolia ‘Brilliantissima’</td>
            <td width="221">Red Chokeberry</td>
            <td width="79">2 GAL</td>
            <td width="71">10.00</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">BERBERIS</td>
            <td width="245">Berberis Thunbergii ‘Concorde’</td>
            <td width="221">Concorde Barberry</td>
            <td width="79">3 GAL</td>
            <td width="71">20.00</td>
            <td width="69">19.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">BERBERIS</td>
            <td width="245">Berberis Thunbergii ‘Rose Glow’</td>
            <td width="221">Rose Glow Barberry</td>
            <td width="79">3 GAL</td>
            <td width="71">20.00</td>
            <td width="69">19.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CEANOTHUS</td>
            <td width="245">Ceanothus Americanus</td>
            <td width="221">New Jersey Tea</td>
            <td width="79">2 GAL</td>
            <td width="71">15.00</td>
            <td width="69">14.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CERCIS</td>
            <td width="245">Cercis Canadensis</td>
            <td width="221">Eastern Redbud</td>
            <td width="79">150cm</td>
            <td width="71">135.00</td>
            <td width="69">125.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CERCIS</td>
            <td width="245">Cercis Canadensis</td>
            <td width="221">Eastern Redbud</td>
            <td width="79">3 GAL</td>
            <td width="71">16.50</td>
            <td width="69">15.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CLETHRA</td>
            <td width="245">Clethra Alnifolia ‘Hummingbird’</td>
            <td width="221">Hummingbird Summersweet</td>
            <td width="79">2 GAL</td>
            <td width="71">11.25</td>
            <td width="69">10.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CLETHRA</td>
            <td width="245">Clethra Alnifolia ‘Ruby Spice’</td>
            <td width="221">Ruby Spice Summersweet</td>
            <td width="79">2 GAL</td>
            <td width="71">11.25</td>
            <td width="69">10.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CORNUS</td>
            <td width="245">Cornus Alba ‘Bailho’</td>
            <td width="221">Ivory Halo ‘Dogwood’</td>
            <td width="79">2 GAL</td>
            <td width="71">10.75</td>
            <td width="69">9.95</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CORNUS</td>
            <td width="245">Cornus Alba ‘Elegantissima’</td>
            <td width="221">Silver Edge Dogwood</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CORNUS</td>
            <td width="245">Cornus Alternifolia</td>
            <td width="221">Pagoda Dogwood</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CORNUS</td>
            <td width="245">Cornus Racemosa</td>
            <td width="221">Gray Dogwood</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CORNUS</td>
            <td width="245">Cornus Sericea</td>
            <td width="221">Red Oiser Dogwood</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CORNUS</td>
            <td width="245">Cornus Sericea ‘Flaviramea’</td>
            <td width="221">Yellowtwig Dogwood</td>
            <td width="79">2 GAL</td>
            <td width="71">10.75</td>
            <td width="69">9.95</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">CORNUS</td>
            <td width="245">Cornus Sericea ‘Kelsey’</td>
            <td width="221">Kelsey Dwarf Dogwood</td>
            <td width="79">2 GAL</td>
            <td width="71">10.25</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">COTINUS</td>
            <td width="245">Cotinus Coggygria ‘Royal Purple’</td>
            <td width="221">Royal Purple Smokebush</td>
            <td width="79">3 GAL</td>
            <td width="71">16.50</td>
            <td width="69">15.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">COTONEASTER</td>
            <td width="245">Cotoneaster Acutifolius</td>
            <td width="221">Peking Cotoneaster</td>
            <td width="79">2 GAL</td>
            <td width="71">10.75</td>
            <td width="69">9.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">COTONEASTER</td>
            <td width="245">Cotoneaster Apiculates</td>
            <td width="221">Cranberry Cotoneaster</td>
            <td width="79">2 GAL</td>
            <td width="71">10.75</td>
            <td width="69">9.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">COTONEASTER</td>
            <td width="245">Cotoneaster Horizontalis</td>
            <td width="221">Rock Spray/Fishbone Cotoneaster</td>
            <td width="79">2 GAL</td>
            <td width="71">10.75</td>
            <td width="69">9.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">DEUTZIA</td>
            <td width="245">Deutzia Gracilis</td>
            <td width="221">Slender Deutzia</td>
            <td width="79">2 GAL</td>
            <td width="71">11.25</td>
            <td width="69">9.95</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">DIERVILLA</td>
            <td width="245">Diervilla Lonicera</td>
            <td width="221">Bush Honeysuckle</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus Alatus</td>
            <td width="221">Winged Burning Bush</td>
            <td width="79">3 GAL</td>
            <td width="71">17.00</td>
            <td width="69">16.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">EUONYMUS</td>
            <td width="245">Euonymus Alatus ‘Compactus’</td>
            <td width="221">Dwarf Winged Burning Bush</td>
            <td width="79">3 GAL</td>
            <td width="71">17.00</td>
            <td width="69">16.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">FORSYTHIA</td>
            <td width="245">Forsythia x ‘Courtasol</td>
            <td width="221">Gold Tide Forsythia</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">FORSYTHIA</td>
            <td width="245">Forsythia x Intermedia ‘Spectabilis’</td>
            <td width="221">Showy Forsythia</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">FORSYTHIA</td>
            <td width="245">Forsythia x Ovata ‘Nothern Gold’</td>
            <td width="221">Northern Gold Forsythia</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">GENISTA</td>
            <td width="245">Genista Lydia</td>
            <td width="221">Woadwaxen</td>
            <td width="79">2 GAL</td>
            <td width="71">14.00</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HAMAMELIS</td>
            <td width="245">Hamamelis Virginiana</td>
            <td width="221">Common Witch Hazel</td>
            <td width="79">150cm W.B.</td>
            <td width="71">135.00</td>
            <td width="69">125.00</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HAMAMELIS</td>
            <td width="245">Hamamelis Virginiana</td>
            <td width="221">Common Witch Hazel</td>
            <td width="79">3 GAL</td>
            <td width="71">19.00</td>
            <td width="69">18.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HIBISCUS</td>
            <td width="245">Hibiscus Double Flowering</td>
            <td width="221">Hibiscus syriacus ‘Ardens’ – Purple</td>
            <td width="79">3 GAL</td>
            <td width="71">15.25</td>
            <td width="69">14.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HIBISCUS</td>
            <td width="245">Hibiscus Single Flowering</td>
            <td width="221">Hibiscus syriacus ‘Aphrodite’ – Dark pink</td>
            <td width="79">3 GAL</td>
            <td width="71">15.25</td>
            <td width="69">14.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HIBISCUS</td>
            <td width="245">Hibiscus Single Flowering</td>
            <td width="221">Hibiscus syriacus ‘Diana’ – White</td>
            <td width="79">3 GAL</td>
            <td width="71">15.25</td>
            <td width="69">14.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HIBISCUS</td>
            <td width="245">Hibiscus Single Flowering</td>
            <td width="221">Hibiscus syriacus ‘Red Heart’ – Single pure white</td>
            <td width="79">3 GAL</td>
            <td width="71">15.25</td>
            <td width="69">14.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HIBISCUS</td>
            <td width="245">Hibiscus Syriacus</td>
            <td width="221">Rose of Sharon</td>
            <td width="79">3 GAL</td>
            <td width="71">15.25</td>
            <td width="69">14.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Arborescens ‘Annabelle’</td>
            <td width="221">Annabelle Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">10.75</td>
            <td width="69">9.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Arborescens ‘Annabelle’</td>
            <td width="221">Annabelle Hydrangea</td>
            <td width="79">5 GAL</td>
            <td width="71">23.50</td>
            <td width="69">21.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Macrophylla ‘ Nikko Blue’</td>
            <td width="221">Nikko Blue Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">15.50</td>
            <td width="69">14.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Macrophylla ‘Bailmer’</td>
            <td width="221">Endless Summer Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">21.00</td>
            <td width="69">19.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Macrophylla ‘Bailmer’</td>
            <td width="221">Endless Summer Hydrangea</td>
            <td width="79">5 GAL</td>
            <td width="71">32.00</td>
            <td width="69">28.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Macrophylla ‘Cityline Berlin’</td>
            <td width="221">Berlin Bigleaf Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">21.50</td>
            <td width="69">19.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Macrophylla ‘Cityline Paris’</td>
            <td width="221">Paris Bigleaf Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">21.50</td>
            <td width="69">19.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Macrophylla ‘Cityline Ramars’</td>
            <td width="221">Mars Bigleaf Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">21.50</td>
            <td width="69">19.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Macrophylla ‘Cityline Venice’</td>
            <td width="221">Venice Bigleaf Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">21.50</td>
            <td width="69">19.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Macrophylla ‘Cityline Vienna’</td>
            <td width="221">Vienna Bigleaf Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">21.50</td>
            <td width="69">19.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘DV Pinky’</td>
            <td width="221">Pinky Winky Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">17.00</td>
            <td width="69">15.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Jane’</td>
            <td width="221">Little Lime Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">17.50</td>
            <td width="69">15.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Limelight’</td>
            <td width="221">Limelight Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">17.50</td>
            <td width="69">15.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Limelight’</td>
            <td width="221">Limelight Hydrangea</td>
            <td width="79">3 GAL</td>
            <td width="71">19.50</td>
            <td width="69">18.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Pee Wee’</td>
            <td width="221">Pee Wee Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">17.00</td>
            <td width="69">15.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Quercifolia</td>
            <td width="221">Oakleaf Hydrangea</td>
            <td width="79">2 GAL</td>
            <td width="71">18.50</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">HYPERICUM</td>
            <td width="245">Hypericum Kalmianum</td>
            <td width="221">Kalm St.Johns Wort</td>
            <td width="79">2 GAL</td>
            <td width="71">10.75</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ILEX</td>
            <td width="245">Ilex Verticillata</td>
            <td width="221">Winterberry</td>
            <td width="79">2 GAL</td>
            <td width="71">11.50</td>
            <td width="69">10.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">KERRIA</td>
            <td width="245">Kerria Japonica ‘Pleniflora’</td>
            <td width="221">Japanese Rose</td>
            <td width="79">2 GAL</td>
            <td width="71">14.75</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">LIGUSTRUM</td>
            <td width="245">Ligustrum Amurense</td>
            <td width="221">Amur Privit</td>
            <td width="79">2 GAL</td>
            <td width="71">10.50</td>
            <td width="69">9.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">LIGUSTRUM</td>
            <td width="245">Ligustrum x Vicaryi</td>
            <td width="221">Golden Privit</td>
            <td width="79">2 GAL</td>
            <td width="71">10.50</td>
            <td width="69">9.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">LONICERA</td>
            <td width="245">Lonicera Morrowii</td>
            <td width="221">Morrow Honeysuckle</td>
            <td width="79">2 GAL</td>
            <td width="71">11.50</td>
            <td width="69">10.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">LONICERA</td>
            <td width="245">Lonicera x Xylosteoides ‘Claveyi’</td>
            <td width="221">Clavey’s Dwarf Honeysuckle</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">MYRICA</td>
            <td width="245">Myrica Gale</td>
            <td width="221">Sweet Gale</td>
            <td width="79">2 GAL</td>
            <td width="71">13.25</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">MYRICA</td>
            <td width="245">Myrica Pensylvanica</td>
            <td width="221">Bayberry</td>
            <td width="79">2 GAL</td>
            <td width="71">13.25</td>
            <td width="69">11.75</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PHILADELPHUS</td>
            <td width="245">Philadelphus Coronarius ‘Aureus’</td>
            <td width="221">Golden Mockorange</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PHILADELPHUS</td>
            <td width="245">Philadelphus x Virginalis ‘Minnesota Snowflake’</td>
            <td width="221">Minnesota Snowflake Mockorange</td>
            <td width="79">2 GAL</td>
            <td width="71">11.25</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PHYSOCARPUS</td>
            <td width="245">Physocarpus Opulifolius</td>
            <td width="221">Common Ninebark</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PHYSOCARPUS</td>
            <td width="245">Physocarpus Opulifolius ‘Dart’s Gold’</td>
            <td width="221">Dart’s Gold Ninebark</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PHYSOCARPUS</td>
            <td width="245">Physocarpus Opulifolius ‘Monlo’</td>
            <td width="221">Diabolo Ninebark</td>
            <td width="79">2 GAL</td>
            <td width="71">12.00</td>
            <td width="69">10.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PHYSOCARPUS</td>
            <td width="245">Physocarpus Opulifolius ‘Nanus’</td>
            <td width="221">Dwarf Ninebark</td>
            <td width="79">2 GAL</td>
            <td width="71">10.50</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PHYSOCARPUS</td>
            <td width="245">Physocarpus Opulifolius ‘Seward’</td>
            <td width="221">Summer Wine Ninebark</td>
            <td width="79">2 GAL</td>
            <td width="71">14.75</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">POTENTILLA</td>
            <td width="245">Pontentilla Fruticosa ‘ Pink Beauty’</td>
            <td width="221">Pink Beauty Pontentilla</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">POTENTILLA</td>
            <td width="245">Potentilla Fruticosa ‘Abbotswood’</td>
            <td width="221">Abbotswood Potentilla</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">POTENTILLA</td>
            <td width="245">Potentilla Fruticosa ‘Goldfinger’</td>
            <td width="221">Goldfinger Potentilla</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">POTENTILLA</td>
            <td width="245">Potentilla Fruticosa ‘Goldstar’</td>
            <td width="221">Goldstar Potentilla</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PRUNUS</td>
            <td width="245">Prunus Virginiana</td>
            <td width="221">Common Chokecherry</td>
            <td width="79">2 GAL</td>
            <td width="71">12.00</td>
            <td width="69">10.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">PRUNUS</td>
            <td width="245">Prunus x Cistena</td>
            <td width="221">Purple Leaf Sand Cherry</td>
            <td width="79">2 GAL</td>
            <td width="71">12.00</td>
            <td width="69">10.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">RHUS</td>
            <td width="245">Rhus Aromatica</td>
            <td width="221">Fragrant Sumac</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">RHUS</td>
            <td width="245">Rhus Aromatica ‘ Gro-Low’</td>
            <td width="221">Gro-Low Sumac</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">RHUS</td>
            <td width="245">Rhus Typhina</td>
            <td width="221">Staghorn Sumac</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">RIBES</td>
            <td width="245">Ribes Alpinum</td>
            <td width="221">Apline Current</td>
            <td width="79">2 GAL</td>
            <td width="71">11.50</td>
            <td width="69">9.95</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">RIBES</td>
            <td width="245">Ribes Aureum</td>
            <td width="221">Golden Current</td>
            <td width="79">2 GAL</td>
            <td width="71">11.50</td>
            <td width="69">9.95</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ROSA</td>
            <td width="245">Rosa Blanda</td>
            <td width="221">Meadow Rose</td>
            <td width="79">2 GAL</td>
            <td width="71">12.25</td>
            <td width="69">10.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ROSA</td>
            <td width="245">Rosa Carolina</td>
            <td width="221">Pasture Rose</td>
            <td width="79">2 GAL</td>
            <td width="71">12.25</td>
            <td width="69">10.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ROSA</td>
            <td width="245">Rosa Rugosa</td>
            <td width="221">Japanese Rose</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">ROSA</td>
            <td width="245">Rosa Rugosa Alba</td>
            <td width="221">White Japanese Rose</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">RUBUS</td>
            <td width="245">Rubus Odoratus</td>
            <td width="221">Flowering Raspberry</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SALIX</td>
            <td width="245">Salix Bebbiana</td>
            <td width="221">Becked Willow</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SALIX</td>
            <td width="245">Salix Discolor</td>
            <td width="221">Pussy Willow</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SALIX</td>
            <td width="245">Salix Purpurpea ‘Nana/Gracilis’</td>
            <td width="221">Blue Arctic Willow</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SAMBUCUS</td>
            <td width="245">Sambucus Canadensis</td>
            <td width="221">American Elder</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SAMBUCUS</td>
            <td width="245">Sambucus Pubens</td>
            <td width="221">Scarlet Elder</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SORBARIA</td>
            <td width="245">Sorbaria Sorbifolia</td>
            <td width="221">False Spirea</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SPIRAEA</td>
            <td width="245">Spiraea Alba</td>
            <td width="221">Meadowsweet</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SPIRAEA</td>
            <td width="245">Spiraea Japonica ‘Froebelii’</td>
            <td width="221">Frobel’s Spiraea</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SPIRAEA</td>
            <td width="245">Spiraea Japonica ‘Goldflame’</td>
            <td width="221">Goldflame Spiraea</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SPIRAEA</td>
            <td width="245">Spiraea Japonica ‘Little Princess’</td>
            <td width="221">Little Princess Spiraea</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SPIRAEA</td>
            <td width="245">Spiraea x Bumalda ‘Dart’s Red’</td>
            <td width="221">Dart’s Red Spiraea</td>
            <td width="79">2 GAL</td>
            <td width="71">9.25</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SPIRAEA</td>
            <td width="245">Spiraea x ‘Gold Mound’</td>
            <td width="221">Gold Mound Spiraea</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SPIRAEA</td>
            <td width="245">Spiraea x Vanhouttei</td>
            <td width="221">Vanhouttei Spiraea</td>
            <td width="79">2 GAL</td>
            <td width="71">9.25</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">STEPHANANDRA</td>
            <td width="245">Stephanandra Incisa ‘Crispa’</td>
            <td width="221">Cutleaf Stephanandra</td>
            <td width="79">2 GAL</td>
            <td width="71">12.25</td>
            <td width="69">10.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SYMPHORICARPOS</td>
            <td width="245">Symphoricarpos Albus</td>
            <td width="221">White Snowberry</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Meyeri ‘Palibin’</td>
            <td width="221">Dwarf Korean Lilac</td>
            <td width="79">2 GAL</td>
            <td width="71">14.50</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Patula ‘Miss Kim’</td>
            <td width="221">Miss Kim Lilac</td>
            <td width="79">2 GAL</td>
            <td width="71">14.50</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Vulgaris</td>
            <td width="221">Common Lilac</td>
            <td width="79">2 GAL</td>
            <td width="71">14.50</td>
            <td width="69">12.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Vulgaris ‘French Hybrid’</td>
            <td width="221">French Hybrid Lilacs</td>
            <td width="79">3 GAL</td>
            <td width="71">18.50</td>
            <td width="69">17.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa x ‘Penda’</td>
            <td width="221">Bloomerang Lilac</td>
            <td width="79">2 GAL</td>
            <td width="71">23.50</td>
            <td width="69">21.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">VIBURNUM</td>
            <td width="245">Viburnum Cassinoides</td>
            <td width="221">Witherod</td>
            <td width="79">2 GAL</td>
            <td width="71">14.00</td>
            <td width="69">12.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">VIBURNUM</td>
            <td width="245">Viburnum Dentatum</td>
            <td width="221">Arrow Wood</td>
            <td width="79">2 GAL</td>
            <td width="71">11.00</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">VIBURNUM</td>
            <td width="245">Viburnum Lentago</td>
            <td width="221">Nannyberry</td>
            <td width="79">2 GAL</td>
            <td width="71">11.00</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">VIBURNUM</td>
            <td width="245">Viburnum Trilobum</td>
            <td width="221">Highbush Cranberry</td>
            <td width="79">2 GAL</td>
            <td width="71">11.00</td>
            <td width="69">9.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">WEIGELA</td>
            <td width="245">Weigela florida ‘ Alexandra’</td>
            <td width="221">Wine and Roses Weigela</td>
            <td width="79">2 GAL</td>
            <td width="71">14.50</td>
            <td width="69">12.50</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">WEIGELA</td>
            <td width="245">Weigela Florida ‘ Minuet’</td>
            <td width="221">Minuet Weigela</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">WEIGELA</td>
            <td width="245">Weigela Florida ‘Bristol Ruby’</td>
            <td width="221">Bristol Ruby Weigela</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">SHRUBS</td>
            <td width="181">WEIGELA</td>
            <td width="245">Weigela Florida ‘Red Prince’</td>
            <td width="221">Red Prince Weigela</td>
            <td width="79">2 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagua Sylvatica ‘Dawyk’s Gold’</td>
            <td width="221">Dawyk’s Gold Beech</td>
            <td width="79">50mm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagua Sylvatica ‘Dawyk’s Gold’</td>
            <td width="221">Dawyk’s Gold Beech</td>
            <td width="79">60mm</td>
            <td width="71">300.00</td>
            <td width="69">275.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica</td>
            <td width="221">Eurpoean Beech</td>
            <td width="79">50mm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica</td>
            <td width="221">Eurpoean Beech</td>
            <td width="79">60mm</td>
            <td width="71">300.00</td>
            <td width="69">275.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Dawyk’s Purple’</td>
            <td width="221">Dawyk’s Pruple Beech</td>
            <td width="79">50mm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Dawyk’s Purple’</td>
            <td width="221">Dawyk’s Pruple Beech</td>
            <td width="79">60mm</td>
            <td width="71">300.00</td>
            <td width="69">275.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Purple Fountain’</td>
            <td width="221">Purple Fountain Beech</td>
            <td width="79">50mm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Purple Fountain’</td>
            <td width="221">Purple Fountain Beech</td>
            <td width="79">60mm</td>
            <td width="71">300.00</td>
            <td width="69">275.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Red Obelisk’</td>
            <td width="221">Red Obelisk Beech</td>
            <td width="79">50mm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Red Obelisk’</td>
            <td width="221">Red Obelisk Beech</td>
            <td width="79">60mm</td>
            <td width="71">300.00</td>
            <td width="69">275.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Riversii’</td>
            <td width="221">Rivers Pruple Beech</td>
            <td width="79">50mm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Riversii’</td>
            <td width="221">Rivers Pruple Beech</td>
            <td width="79">60mm</td>
            <td width="71">300.00</td>
            <td width="69">275.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Tricolor’</td>
            <td width="221">Tricolor Beech</td>
            <td width="79">50mm</td>
            <td width="71">260.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BEECH</td>
            <td width="245">Fagus Sylvatica ‘Tricolor’</td>
            <td width="221">Tricolor Beech</td>
            <td width="79">60mm</td>
            <td width="71">300.00</td>
            <td width="69">275.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BIRCH</td>
            <td width="245">Betula Nigra</td>
            <td width="221">River Birch</td>
            <td width="79">45mm</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BIRCH</td>
            <td width="245">Betula Nigra</td>
            <td width="221">River Birch</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BIRCH</td>
            <td width="245">Betula Nigra</td>
            <td width="221">River Birch</td>
            <td width="79">60mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BIRCH</td>
            <td width="245">Betula Papyrifera</td>
            <td width="221">Canoe or Paper Birch</td>
            <td width="79">45mm</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BIRCH</td>
            <td width="245">Betula Papyrifera</td>
            <td width="221">Canoe or Paper Birch</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">BIRCH</td>
            <td width="245">Betula Papyrifera</td>
            <td width="221">Canoe or Paper Birch</td>
            <td width="79">60mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Armstrong’</td>
            <td width="221">Armstrong Red Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Armstrong’</td>
            <td width="221">Armstrong Red Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Armstrong’</td>
            <td width="221">Armstrong Red Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Bowhall’</td>
            <td width="221">Bowhall Red Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Bowhall’</td>
            <td width="221">Bowhall Red Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Bowhall’</td>
            <td width="221">Bowhall Red Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharinum</td>
            <td width="221">Silver Maple</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharinum</td>
            <td width="221">Silver Maple</td>
            <td width="79">60mm</td>
            <td width="71">205.00</td>
            <td width="69">195.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharinum</td>
            <td width="221">Silver Maple</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharum</td>
            <td width="221">Sugar Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharum</td>
            <td width="221">Sugar Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharum</td>
            <td width="221">Sugar Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharum ‘Green Mountain’</td>
            <td width="221">Green Mountain Sugar Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharum ‘Green Mountain’</td>
            <td width="221">Green Mountain Sugar Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharum ‘Green Mountain’</td>
            <td width="221">Green Mountain Sugar Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharum ‘Endowment’</td>
            <td width="221">Endowment Sugar Maple</td>
            <td width="79">50mm</td>
            <td width="71">225.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Saccharum ‘Endowment’</td>
            <td width="221">Endowment Sugar Maple</td>
            <td width="79">60mm</td>
            <td width="71">255.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">DIOICUS</td>
            <td width="245">Gymnocladus Dioicus</td>
            <td width="221">Kentucky Coffee Tree</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">DIOICUS</td>
            <td width="245">Gymnocladus Dioicus</td>
            <td width="221">Kentucky Coffee Tree</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">DIOICUS</td>
            <td width="245">Gymnocladus Dioicus</td>
            <td width="221">Kentucky Coffee Tree</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">GINKGO</td>
            <td width="245">Ginkgo Biloba</td>
            <td width="221">Maidenhair Tree</td>
            <td width="79">50mm</td>
            <td width="71">245.00</td>
            <td width="69">230.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">GINKGO</td>
            <td width="245">Ginkgo Biloba</td>
            <td width="221">Maidenhair Tree</td>
            <td width="79">60mm</td>
            <td width="71">270.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">GINKGO</td>
            <td width="245">Ginkgo Biloba</td>
            <td width="221">Maidenhair Tree</td>
            <td width="79">70mm</td>
            <td width="71">320.00</td>
            <td width="69">300.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HACKBERRY</td>
            <td width="245">Celtis Occidentalis</td>
            <td width="221">Common Hackberry</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HACKBERRY</td>
            <td width="245">Celtis Occidentalis</td>
            <td width="221">Common Hackberry</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HACKBERRY</td>
            <td width="245">Celtis Occidentalis</td>
            <td width="221">Common Hackberry</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HACKBERRY</td>
            <td width="245">Ceris Canadensis</td>
            <td width="221">Eastern Redbud</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HACKBERRY</td>
            <td width="245">Ceris Canadensis</td>
            <td width="221">Eastern Redbud</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HORNBEAM</td>
            <td width="245">Carpinus Betulus Fastigiata</td>
            <td width="221">Pyramidal European Hornbeam</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HORNBEAM</td>
            <td width="245">Carpinus Betulus Fastigiata</td>
            <td width="221">Pyramidal European Hornbeam</td>
            <td width="79">60mm</td>
            <td width="71">245.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HORNBEAM</td>
            <td width="245">Carpinus Betulus Fastigiata</td>
            <td width="221">Pyramidal European Hornbeam</td>
            <td width="79">70mm</td>
            <td width="71">265.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HORNBEAM</td>
            <td width="245">Carpinus Caroliniana</td>
            <td width="221">Hornbeam, Bluebeech</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HORNBEAM</td>
            <td width="245">Carpinus Caroliniana</td>
            <td width="221">Hornbeam, Bluebeech</td>
            <td width="79">60mm</td>
            <td width="71">245.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HORNBEAM</td>
            <td width="245">Carpinus Caroliniana</td>
            <td width="221">Hornbeam, Bluebeech</td>
            <td width="79">70mm</td>
            <td width="71">265.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HORSE CHESTNUT</td>
            <td width="245">Aesculus Glabra</td>
            <td width="221">Ohio Buckeye</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HORSE CHESTNUT</td>
            <td width="245">Aesculus Glabra</td>
            <td width="221">Ohio Buckeye</td>
            <td width="79">60mm</td>
            <td width="71">235.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrandea Paniculata ‘Grandiflora’ STD.</td>
            <td width="221">Peegee Hydrangea</td>
            <td width="79">10 GAL</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrandea Paniculata ‘Grandiflora’ STD.</td>
            <td width="221">Peegee Hydrangea</td>
            <td width="79">7 GAL</td>
            <td width="71">85.00</td>
            <td width="69">75.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Bulk’ STD.</td>
            <td width="221">Quick Fire Hydrangea</td>
            <td width="79">10 GAL</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Bulk’ STD.</td>
            <td width="221">Quick Fire Hydrangea</td>
            <td width="79">7 GAL</td>
            <td width="71">85.00</td>
            <td width="69">75.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘DVPinky’ STD.</td>
            <td width="221">Pinky Winky Hydrangea</td>
            <td width="79">10 GAL</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘DVPinky’ STD.</td>
            <td width="221">Pinky Winky Hydrangea</td>
            <td width="79">7 GAL</td>
            <td width="71">85.00</td>
            <td width="69">75.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Grandiflora Standard’</td>
            <td width="221">Peegee Hydrangea Standard</td>
            <td width="79">10 GAL</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Grandiflora Standard’</td>
            <td width="221">Peegee Hydrangea Standard</td>
            <td width="79">7 GAL</td>
            <td width="71">85.00</td>
            <td width="69">75.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Limelight’ STD.</td>
            <td width="221">Limelight Hydrangea</td>
            <td width="79">10 GAL</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Paniculata ‘Limelight’ STD.</td>
            <td width="221">Limelight Hydrangea</td>
            <td width="79">7 GAL</td>
            <td width="71">85.00</td>
            <td width="69">75.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Shademaster’</td>
            <td width="221">Shademaster Locust</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Shademaster’</td>
            <td width="221">Shademaster Locust</td>
            <td width="79">60mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Shademaster’</td>
            <td width="221">Shademaster Locust</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">230.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Skycole’</td>
            <td width="221">Skyline Locust</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Skycole’</td>
            <td width="221">Skyline Locust</td>
            <td width="79">60mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Skycole’</td>
            <td width="221">Skyline Locust</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">230.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Suncole’</td>
            <td width="221">Sunburst Locust</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Suncole’</td>
            <td width="221">Sunburst Locust</td>
            <td width="79">60mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LOCUST</td>
            <td width="245">Gleditsia Triacanthos ‘Suncole’</td>
            <td width="221">Sunburst Locust</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">230.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LORIODENDRON</td>
            <td width="245">Liriodendron Tulipifera</td>
            <td width="221">Tulip Tree</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LORIODENDRON</td>
            <td width="245">Liriodendron Tulipifera</td>
            <td width="221">Tulip Tree</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">LORIODENDRON</td>
            <td width="245">Liriodendron Tulipifera</td>
            <td width="221">Tulip Tree</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia Stellata ‘Royal Star’</td>
            <td width="221">Royal Star Magnolia</td>
            <td width="79">175cm W.B.</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia Stellata ‘Royal Star’</td>
            <td width="221">Royal Star Magnolia</td>
            <td width="79">200cm W.B.</td>
            <td width="71">175.00</td>
            <td width="69">165.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia Stellata ‘Royal Star’</td>
            <td width="221">Royal Star Magnolia</td>
            <td width="79">250cm W.B.</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X ‘Betty’</td>
            <td width="221">Betty Magnolia</td>
            <td width="79">175cm W.B.</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X ‘Betty’</td>
            <td width="221">Betty Magnolia</td>
            <td width="79">200cm W.B.</td>
            <td width="71">175.00</td>
            <td width="69">165.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X ‘Betty’</td>
            <td width="221">Betty Magnolia</td>
            <td width="79">250cm W.B.</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X Soulangiana</td>
            <td width="221">Soulangiana Magnolia</td>
            <td width="79">175cm W.B.</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X Soulangiana</td>
            <td width="221">Soulangiana Magnolia</td>
            <td width="79">200cm W.B.</td>
            <td width="71">175.00</td>
            <td width="69">165.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X Soulangiana</td>
            <td width="221">Soulangiana Magnolia</td>
            <td width="79">250cm W.B.</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X ‘Susan’</td>
            <td width="221">Susan Magnolia</td>
            <td width="79">175cm W.B.</td>
            <td width="71">155.00</td>
            <td width="69">145.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X ‘Susan’</td>
            <td width="221">Susan Magnolia</td>
            <td width="79">200cm W.B.</td>
            <td width="71">175.00</td>
            <td width="69">165.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAGNOLIA</td>
            <td width="245">Magnolia X ‘Susan’</td>
            <td width="221">Susan Magnolia</td>
            <td width="79">250cm W.B.</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malas Baccata ‘Columnaris’</td>
            <td width="221">Columnar Crabapple</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malas Baccata ‘Columnaris’</td>
            <td width="221">Columnar Crabapple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malus ‘Makamik’</td>
            <td width="221">Makamik Crabapple</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malus ‘Makamik’</td>
            <td width="221">Makamik Crabapple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malus ‘Prairifire’</td>
            <td width="221">Prairifire Crabapple</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malus ‘Prairifire’</td>
            <td width="221">Prairifire Crabapple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malus ‘Profusion’</td>
            <td width="221">Profusion Crabapple</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malus ‘Profusion’</td>
            <td width="221">Profusion Crabapple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malus ‘Spring Snow’</td>
            <td width="221">Spring Snow Crabapple</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MALUS</td>
            <td width="245">Malus ‘Spring Snow’</td>
            <td width="221">Spring Snow Crabapple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Ginnala</td>
            <td width="221">Amur Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Platanoides ‘Crimson King’</td>
            <td width="221">Crimson King Maple</td>
            <td width="79">50mm</td>
            <td width="71">190.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Platanoides ‘Crimson King’</td>
            <td width="221">Crimson King Maple</td>
            <td width="79">60mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Platanoides ‘Crimson Sentry’</td>
            <td width="221">Crimson Sentry Maple</td>
            <td width="79">50mm</td>
            <td width="71">235.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Platanoides ‘Crimson Sentry’</td>
            <td width="221">Crimson Sentry Maple</td>
            <td width="79">60mm</td>
            <td width="71">265.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum</td>
            <td width="221">Red Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum</td>
            <td width="221">Red Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum</td>
            <td width="221">Red Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Franksred’</td>
            <td width="221">Red Sunset Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Franksred’</td>
            <td width="221">Red Sunset Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘Franksred’</td>
            <td width="221">Red Sunset Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘October Glory’</td>
            <td width="221">October Glory Red Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘October Glory’</td>
            <td width="221">October Glory Red Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer Rubrum ‘October Glory’</td>
            <td width="221">October Glory Red Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer x Freemanii ‘Autumn Blaze’</td>
            <td width="221">Autumn Blaze Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer x Freemanii ‘Autumn Blaze’</td>
            <td width="221">Autumn Blaze Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer x Freemanii ‘Autumn Blaze’</td>
            <td width="221">Autumn Blaze Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer x Freemanii ‘Autumn Fantasy’</td>
            <td width="221">Autumn Fantasy Maple</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer x Freemanii ‘Autumn Fantasy’</td>
            <td width="221">Autumn Fantasy Maple</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">MAPLE</td>
            <td width="245">Acer x Freemanii ‘Autumn Fantasy’</td>
            <td width="221">Autumn Fantasy Maple</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Alba</td>
            <td width="221">White Oak</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">195.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Alba</td>
            <td width="221">White Oak</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Alba</td>
            <td width="221">White Oak</td>
            <td width="79">70mm</td>
            <td width="71">270.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Macrocarpa</td>
            <td width="221">Burr Oak</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">195.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Macrocarpa</td>
            <td width="221">Burr Oak</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Macrocarpa</td>
            <td width="221">Burr Oak</td>
            <td width="79">70mm</td>
            <td width="71">270.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Palustris</td>
            <td width="221">Pin Oak</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">195.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Palustris</td>
            <td width="221">Pin Oak</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Palustris</td>
            <td width="221">Pin Oak</td>
            <td width="79">70mm</td>
            <td width="71">270.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Robur ‘Fastigiata’</td>
            <td width="221">Pyramidal English Oak</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">195.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Robur ‘Fastigiata’</td>
            <td width="221">Pyramidal English Oak</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Robur ‘Fastigiata’</td>
            <td width="221">Pyramidal English Oak</td>
            <td width="79">70mm</td>
            <td width="71">270.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Robur ‘Regal Prince’</td>
            <td width="221">Regal Prince Oak</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">195.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Robur ‘Regal Prince’</td>
            <td width="221">Regal Prince Oak</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Robur ‘Regal Prince’</td>
            <td width="221">Regal Prince Oak</td>
            <td width="79">70mm</td>
            <td width="71">270.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Rubra</td>
            <td width="221">Red Oak</td>
            <td width="79">50mm</td>
            <td width="71">215.00</td>
            <td width="69">195.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Rubra</td>
            <td width="221">Red Oak</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">225.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OAK</td>
            <td width="245">Quercus Rubra</td>
            <td width="221">Red Oak</td>
            <td width="79">70mm</td>
            <td width="71">270.00</td>
            <td width="69">255.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">OSTRYA</td>
            <td width="245">Ostrya Virginiana</td>
            <td width="221">Hophornbeam, Ironwood</td>
            <td width="79">50mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PLATANUS</td>
            <td width="245">Platanus Acerifolia ‘Bloodgood’</td>
            <td width="221">Bloodgood London Planetree</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PLATANUS</td>
            <td width="245">Platanus Acerifolia ‘Bloodgood’</td>
            <td width="221">Bloodgood London Planetree</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PLATANUS</td>
            <td width="245">Platanus Occidentalis</td>
            <td width="221">Planetree, American Sycamore</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PLATANUS</td>
            <td width="245">Platanus Occidentalis</td>
            <td width="221">Planetree, American Sycamore</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">POPULUS</td>
            <td width="245">Populus Tremula Erecta</td>
            <td width="221">Swedish Columnar Aspen</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">POPULUS</td>
            <td width="245">Populus Tremula Erecta</td>
            <td width="221">Swedish Columnar Aspen</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">POPULUS</td>
            <td width="245">Populus Tremuloides</td>
            <td width="221">Trembling Aspen</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">POPULUS</td>
            <td width="245">Populus Tremuloides</td>
            <td width="221">Trembling Aspen</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Bradford’</td>
            <td width="221">Bradford Pear</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Bradford’</td>
            <td width="221">Bradford Pear</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Bradford’</td>
            <td width="221">Bradford Pear</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Capital’</td>
            <td width="221">Capital Ornamental Pear</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Capital’</td>
            <td width="221">Capital Ornamental Pear</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Capital’</td>
            <td width="221">Capital Ornamental Pear</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Chanticleer’</td>
            <td width="221">Chanticleer Pear</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Chanticleer’</td>
            <td width="221">Chanticleer Pear</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Chanticleer’</td>
            <td width="221">Chanticleer Pear</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Redspire’</td>
            <td width="221">Redspire Pear</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Redspire’</td>
            <td width="221">Redspire Pear</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">PYRUS</td>
            <td width="245">Pyrus Calleryana ‘Redspire’</td>
            <td width="221">Redspire Pear</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SALIX</td>
            <td width="245">Salix Alba Tristis</td>
            <td width="221">Golden Weeping Willow</td>
            <td width="79">45mm</td>
            <td width="71">165.00</td>
            <td width="69">155.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SALIX</td>
            <td width="245">Salix Alba Tristis</td>
            <td width="221">Golden Weeping Willow</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SALIX</td>
            <td width="245">Salix Alba Tristis</td>
            <td width="221">Golden Weeping Willow</td>
            <td width="79">60mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SERVICEBERRY</td>
            <td width="245">Amelanchier Canadensis</td>
            <td width="221">Serviceberry-Juneberry</td>
            <td width="79">50mm</td>
            <td width="71">200.00</td>
            <td width="69">190.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SERVICEBERRY</td>
            <td width="245">Amelanchier Canadensis</td>
            <td width="221">Serviceberry-Juneberry</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">220.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SERVICEBERRY</td>
            <td width="245">Amelanchier Grandiflora ‘Autumn Brilliance’</td>
            <td width="221">Autumn Brilliance Serviceberry</td>
            <td width="79">50mm</td>
            <td width="71">200.00</td>
            <td width="69">190.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SERVICEBERRY</td>
            <td width="245">Amelanchier Grandiflora ‘Autumn Brilliance’</td>
            <td width="221">Autumn Brilliance Serviceberry</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">220.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SERVICEBERRY</td>
            <td width="245">Amelanchier Grandiflora ‘Robin Hill’</td>
            <td width="221">Robin Hill Serviceberry</td>
            <td width="79">50mm</td>
            <td width="71">200.00</td>
            <td width="69">190.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SERVICEBERRY</td>
            <td width="245">Amelanchier Grandiflora ‘Robin Hill’</td>
            <td width="221">Robin Hill Serviceberry</td>
            <td width="79">60mm</td>
            <td width="71">230.00</td>
            <td width="69">220.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SORBUS</td>
            <td width="245">Sorbus Aucuparia ‘Cardinal Royal’</td>
            <td width="221">Cardianal Royal Mountain Ash</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SORBUS</td>
            <td width="245">Sorbus Aucuparia ‘Cardinal Royal’</td>
            <td width="221">Cardianal Royal Mountain Ash</td>
            <td width="79">60mm</td>
            <td width="71">215.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Meyeri ‘Palabin’</td>
            <td width="221">Palabin Lilac STD.</td>
            <td width="79">10 GAL</td>
            <td width="71">95.00</td>
            <td width="69">85.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Meyeri ‘Palabin’</td>
            <td width="221">Palabin Lilac STD.</td>
            <td width="79">7 GAL</td>
            <td width="71">85.00</td>
            <td width="69">75.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Reticulata ‘Ivory Silk’</td>
            <td width="221">Ivory Silk Tree Lilac</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Reticulata ‘Ivory Silk’</td>
            <td width="221">Ivory Silk Tree Lilac</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">SYRINGA</td>
            <td width="245">Syringa Reticulata ‘Ivory Silk’</td>
            <td width="221">Ivory Silk Tree Lilac</td>
            <td width="79">70mm</td>
            <td width="71">245.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Americana</td>
            <td width="221">American Basswood</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Americana</td>
            <td width="221">American Basswood</td>
            <td width="79">60mm</td>
            <td width="71">220.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Americana</td>
            <td width="221">American Basswood</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Americana ‘Redmond’</td>
            <td width="221">Redmond Linden</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Americana ‘Redmond’</td>
            <td width="221">Redmond Linden</td>
            <td width="79">60mm</td>
            <td width="71">220.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Americana ‘Redmond’</td>
            <td width="221">Redmond Linden</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Cordata ‘Greenspire’</td>
            <td width="221">Greenspire Linden</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Cordata ‘Greenspire’</td>
            <td width="221">Greenspire Linden</td>
            <td width="79">60mm</td>
            <td width="71">220.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Cordata ‘Greenspire’</td>
            <td width="221">Greenspire Linden</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Flavescens ‘Glenleven’</td>
            <td width="221">Glenleven Linden</td>
            <td width="79">50mm</td>
            <td width="71">185.00</td>
            <td width="69">175.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Flavescens ‘Glenleven’</td>
            <td width="221">Glenleven Linden</td>
            <td width="79">60mm</td>
            <td width="71">220.00</td>
            <td width="69">205.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">TILIA</td>
            <td width="245">Tilia Flavescens ‘Glenleven’</td>
            <td width="221">Glenleven Linden</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">235.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana</td>
            <td width="221">American Elm</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana</td>
            <td width="221">American Elm</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana</td>
            <td width="221">American Elm</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana ‘Princeton’</td>
            <td width="221">Princeton Elm</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana ‘Princeton’</td>
            <td width="221">Princeton Elm</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana ‘Princeton’</td>
            <td width="221">Princeton Elm</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana ‘Valley Forge’</td>
            <td width="221">Valley Forge Elm</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana ‘Valley Forge’</td>
            <td width="221">Valley Forge Elm</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Americana ‘Valley Forge’</td>
            <td width="221">Valley Forge Elm</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Japonica X Wilsoniana ‘Morton’</td>
            <td width="221">Accolade Elm</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Japonica X Wilsoniana ‘Morton’</td>
            <td width="221">Accolade Elm</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ULMUS</td>
            <td width="245">Ulmus Japonica X Wilsoniana ‘Morton’</td>
            <td width="221">Accolade Elm</td>
            <td width="79">70mm</td>
            <td width="71">255.00</td>
            <td width="69">245.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ZELKOVA</td>
            <td width="245">Zelkova Serrata ‘Green Vase’</td>
            <td width="221">Green Vase Zelkova</td>
            <td width="79">50mm</td>
            <td width="71">195.00</td>
            <td width="69">185.00</td>
        </tr>
        <tr>
            <td width="137">TREES</td>
            <td width="181">ZELKOVA</td>
            <td width="245">Zelkova Serrata ‘Green Vase’</td>
            <td width="221">Green Vase Zelkova</td>
            <td width="79">60mm</td>
            <td width="71">225.00</td>
            <td width="69">215.00</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">CLEMATIS</td>
            <td width="245">Clematis ‘Blue Angel’</td>
            <td width="221">Blue Angel Clematis</td>
            <td width="79">1 GAL</td>
            <td width="71">14.75</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">CLEMATIS</td>
            <td width="245">Clematis ‘Henryi’</td>
            <td width="221">Henry Clematis</td>
            <td width="79">1 GAL</td>
            <td width="71">14.75</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">CLEMATIS</td>
            <td width="245">Clematis ‘Nelly Moser’</td>
            <td width="221">Nelly Moser Clematis</td>
            <td width="79">1 GAL</td>
            <td width="71">14.75</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">CLEMATIS</td>
            <td width="245">Clematis Virginiana</td>
            <td width="221">Virgin’s Bower</td>
            <td width="79">1 GAL</td>
            <td width="71">9.50</td>
            <td width="69">9.15</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">CLEMATIS</td>
            <td width="245">Clematis ‘Xjackmanii’</td>
            <td width="221">Jackmanni Clematis</td>
            <td width="79">1 GAL</td>
            <td width="71">14.75</td>
            <td width="69">13.25</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">HEDERA</td>
            <td width="245">Hedera Helix ‘Baltica’</td>
            <td width="221">Baltic Ivy</td>
            <td width="79">1 GAL</td>
            <td width="71">7.50</td>
            <td width="69">6.75</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Anomala Petiolaris/Petiolaris</td>
            <td width="221">Climbing Hydrangea</td>
            <td width="79">1 GAL</td>
            <td width="71">12.50</td>
            <td width="69">10.50</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Anomala Petiolaris/Petiolaris</td>
            <td width="221">Climbing Hydrangea</td>
            <td width="79">2GAL</td>
            <td width="71">20.50</td>
            <td width="69">18.50</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Hydrangea Anomala Petiolaris/Petiolaris</td>
            <td width="221">Climbing Hydrangea</td>
            <td width="79">3 GAL</td>
            <td width="71">28.50</td>
            <td width="69">26.50</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">HYDRANGEA</td>
            <td width="245">Lonicera Japonica ‘Halliana’</td>
            <td width="221">Hall’s Honeysuckle</td>
            <td width="79">1 GAL</td>
            <td width="71">9.50</td>
            <td width="69">8.50</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">PARTHENOCISSUS</td>
            <td width="245">Parthenocissus Quinquefolia</td>
            <td width="221">Virginia Creeper</td>
            <td width="79">1 GAL</td>
            <td width="71">8.75</td>
            <td width="69">7.75</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">PARTHENOCISSUS</td>
            <td width="245">Parthenocissus Quinquefolia ‘Engelmannii’</td>
            <td width="221">Engelman’s Ivy</td>
            <td width="79">1 GAL</td>
            <td width="71">8.75</td>
            <td width="69">7.75</td>
        </tr>
        <tr>
            <td width="137">VINES</td>
            <td width="181">PARTHENOCISSUS</td>
            <td width="245">Parthenocissus Tricuspidata ‘Veitchii’</td>
            <td width="221">Boston Ivy</td>
            <td width="79">1 GAL</td>
            <td width="71">8.75</td>
            <td width="69">7.75</td>
        </tr>
    </tbody>
</table>-->

<?php
get_footer();
