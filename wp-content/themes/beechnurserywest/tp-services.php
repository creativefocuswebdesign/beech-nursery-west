<?php
/**
 * Template Name: Services Template
 */

get_header();
?>

<div class="services-page">
    <div class="custom-header-area">
        <div class="title"><h1>Our <span class="blue">Services</span></div>
    </div>
    <div class="content">
        <div class="site-wrapper">
            <div class="row">
                <div class="col-md-12 p-3">
                    <div class="service-full">
                        <p>Beech Nursery West is focused on customer service satisfaction. Whether you are a large commercial developer, professional landscape contractor, garden centre, municipality, property manager or golf course Beech Nursery West has a large fleet that can deliver all products as per your order.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/service-sourcing.jpg" alt="Sourcing" />
                        <div class="service-title">Sourcing</div>
                        <p>Our team at BEECH NURSERY WEST has decades of experience in the landscaping industry. We have come to understand which products are most valued and we do our best to keep the those in stock. BEECH NURSERY WEST understands, however, that your project may require more than what we currently have available on our 45 acre property.</p>
                        <p>Working with our extensive partner network, we are able to quickly replenish our inventories as well as obtain plant varieties and sizes that may not be part of our normal inventory. Sourcing hard-to-find specimens has become our specialty, so please do not hesitate to inquire about the rare or unusual. BEECH NURSERY WEST will do our best to track it down.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service text-right">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/service-delivery.jpg" alt="Delivery" />
                        <div class="service-title">Delivery</div>
                        <p class="right">BEECH NURSERY WEST’s convenient and cost saving delivery fleet includes large boom trucks and trailers, small trucks, and enclosed dry vans. They are available daily to deliver material directly to your job site or yard.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-promo">
            <div class="site-wrapper">
                <div class="catalog-content">
                    <p>Questions? Feel free to reach out to us, we are here to help!</p><a href="<?php echo get_site_url(); ?>/contact" class="custom-button white">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
    <div class="group-of-companies">
        <div class="site-wrapper">
            <div class="title">We Are A <span class="blue"><span class="thick">Group</span></span> Of Companies</div>
        </div>
        <div class="custom-flex-wrapper">
            <div class="company corporate">
                <div class="company-title"><span class="blue">BEECH </span>Nursery Group</div>
                <?php
                    $beechGroup = get_field('beech_nursery_group_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $beechGroup; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://www.beechnurserygroup.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
            <div class="company toronto">
                <div class="company-title"><span class="blue">BEECH </span>Nursery Toronto</div>
                <?php
                    $beechToronto = get_field('beech_nursery_toronto_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $beechToronto; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://beechnursery.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
            <div class="company spading">
                <div class="company-title"><span class="blue">HALL </span>Tree Spading</div>
                <?php
                    $hallSpading = get_field('hall_tree_spading_text', 'option');
                    // do something with $variable
                ?>
                <p><?php echo $hallSpading; ?></p>
                <div class="button-area">
                    <a class="custom-button white" href="https://halltreespading.com" target="_blank">Visit Website</a>
                </div>
                <div class="dashed-border"></div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
